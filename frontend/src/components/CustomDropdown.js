import * as React from "react";

import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Dropdown from "react-bootstrap/Dropdown";
import Alert from "react-bootstrap/Alert";
import FormControl from "react-bootstrap/FormControl";
import { Formik, Form as FormikForm, useField, useFormikContext } from "formik";

const capitalize = (str) => {
  if (typeof str === "string") {
    return str.replace(/^\w/, (c) => c.toUpperCase());
  } else {
    return "";
  }
};
class CustomDropdown extends React.Component {
  constructor(props) {
    super(props);
    this.state = { loading: true, title: "Select an item...", value: {} };
    console.log(props);
    this.setMed = this.setMed.bind(this);
    props.getData().then((data) => {
      this.setState({ data: data });
      this.setState({ loading: false });
      if (this.props.onMount) {
        this.props.onMount(this.setMed);
      }
    });
  }
  CustomToggle = React.forwardRef(({ children, onClick }, ref) => {
    const [field, meta] = useField(this.props);
    return (
      <>
        {meta.touched && meta.error ? (
          <Alert key="alert" variant={"danger"}>
            {meta.error}
          </Alert>
        ) : null}
        <a
          href=""
          ref={ref}
          onClick={(e) => {
            e.preventDefault();
            onClick(e);
          }}
        >
          {children}
          &#x25bc;
        </a>
      </>
    );
  });
  CustomMenu = React.forwardRef(
    ({ children, style, className, "aria-labelledby": labeledBy }, ref) => {
      const [value, setValue] = React.useState("");
      const [field, meta] = useField(this.props);
      return (
        <div
          ref={ref}
          style={style}
          className={className}
          aria-labelledby={labeledBy}
        >
          <FormControl
            {...field}
            autoFocus
            className="mx-3 my-2 w-auto"
            placeholder="Type to filter..."
            onChange={(e) => {
              setValue(e.target.value);
            }}
            value={value}
          />
          <ul className="list-unstyled">
            {React.Children.toArray(children).filter(
              (child) =>
                !value || child.props.children.toLowerCase().startsWith(value)
            )}
          </ul>
        </div>
      );
    }
  );
  setMed(to) {
    if (this.state.data) {
      let newData = [];
      this.state.data.forEach((el, i) => {
        if (el.med_name == to) {
          newData.push({ ...el, active: true });
        } else {
          newData.push(el);
        }
      });
      this.setState({ ...this.state, data: newData, title: to });
    }
  }
  render() {
    return (
      <>
        <Form.Label
          htmlFor={this.props.id || this.props.name}
          style={{ fontSize: 30 }}
        >
          {capitalize(this.props.label)}
        </Form.Label>
        <Dropdown
          onSelect={(e) => {
            const setState = this.setState.bind(this);
            this.props.onSelect(e, this.state, setState);
          }}
        >
          <Dropdown.Toggle
            as={this.CustomToggle}
            id="dropdown-custom-components"
          >
            {capitalize(this.state.title)}
          </Dropdown.Toggle>

          <Dropdown.Menu as={this.CustomMenu}>
            {this.state.loading ? (
              <Dropdown.Item>Loading...</Dropdown.Item>
            ) : (
              this.state.data.map((item) => {
                if (item.active) {
                  return (
                    <Dropdown.Item
                      active
                      key={JSON.stringify(item)}
                      eventKey={JSON.stringify(item)}
                    >
                      {capitalize(item.med_name)}
                    </Dropdown.Item>
                  );
                }
                return (
                  <Dropdown.Item
                    key={JSON.stringify(item)}
                    eventKey={JSON.stringify(item)}
                  >
                    {capitalize(item.med_name)}
                  </Dropdown.Item>
                );
              })
            )}
          </Dropdown.Menu>
        </Dropdown>
      </>
    );
  }
}

export default CustomDropdown;
