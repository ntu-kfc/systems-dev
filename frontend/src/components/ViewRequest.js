import * as React from "react";
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  useLocation,
  useParams,
  useHistory,
} from "react-router-dom";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Table from "react-bootstrap/Table";

import Alert from "react-bootstrap/Alert";
function Reqs({ get, auth, setInfo }) {
  let [refresh, setRefresh] = React.useState(true);
  let [req, setPersc] = React.useState();

  if (refresh) {
    get()
      .then((ret) => setPersc(ret.request))
      .catch((error) => {
        console.log(error);
      });
    setRefresh(false);
  }

  const approve = (user) => {
    allowordeny(user, true);
  };

  const deny = (user) => {
    allowordeny(user, false);
  };

  const allowordeny = (user, allow) => {
    return new Promise((resolve, reject) => {
      var myHeaders = new Headers();
      myHeaders.append("auth-token", auth().token);
      myHeaders.append("Content-Type", "application/json");

      var raw = JSON.stringify({ groups: ["patient"] });

      var requestOptions = {
        method: "POST",
        headers: myHeaders,
        body: raw,
        redirect: "follow",
      };

      fetch(
        `http://ntukfc.me/api/account/staff/${
          allow ? "grant_access" : "deny_access"
        }/${user}`,
        requestOptions
      )
        .then((response) => response.text())
        .then((result) => JSON.parse(result))
        .then((result) => {
          console.log(result);
          setInfo(result.message);
        })
        .catch((error) => console.log(error));
    });
  };

  if (!req) {
    return <h1>loading</h1>;
  } else {
    return (
      <>
        <h2>ID: {req._id}</h2>
        <h2>
          Name: {req.first_name} {req.last_name}
        </h2>
        <h2>Email: {req.email}</h2>
        <h2>Phone: {req.phone_no}</h2>
        <h2>NHS Number: {req.nhs_number}</h2>
        <h2>Reason: {req.reason}</h2>
        <Button onClick={() => approve(req.email)}>approve</Button>
        <Button onClick={() => deny(req.email)}>deny</Button>
      </>
    );
  }
}

function ViewRequest({ auth }) {
  let { reqId } = useParams();
  let [error, setError] = React.useState();
  let [info, setInfo] = React.useState();
  console.log(auth);
  const getReq = () => {
    return new Promise((resolve, reject) => {
      console.log("check 1");
      var myHeaders = new Headers();
      myHeaders.append("auth-token", auth().token);
      myHeaders.append("Content-Type", "application/json");

      var requestOptions = {
        method: "GET",
        headers: myHeaders,
        redirect: "follow",
      };

      fetch(
        `http://ntukfc.me/api/account/staff/account_request/${reqId}`,
        requestOptions
      )
        .then((response) => response.text())
        .then((result) => {
          console.log(JSON.parse(result));
          resolve(JSON.parse(result));
        })
        .catch((error) => {
          console.log(error);
          reject(error);
        });
    });
  };
  return (
    <>
      {info ? (
        <>
          <Alert variant="secondary">{info}</Alert>{" "}
          <Link to="/managerequests">Go back</Link>
        </>
      ) : null}
      {error ? (
        <>
          <Alert variant="secondary">{error}</Alert>
          <Link to="/managerequests">Go back</Link>
        </>
      ) : null}
      {!error && !info ? (
        <Reqs get={getReq} auth={auth} setInfo={setInfo} />
      ) : null}
    </>
  );
}

export default ViewRequest;
