import * as React from "react";
import Alert from "react-bootstrap/Alert";

class CustomList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      error: null,
      data: [],
      noData: false,
      hideCollected: true,
    };
    this.noData = this.noData.bind(this);
    this.setState = this.setState.bind(this);
  }
  componentDidMount() {
    this.props
      .auth()
      .makeReq(true, this.props.url, "GET")
      .then((data) => {
        if (data.status === 200) {
          if (this.props.modData) data = this.props.modData(data);
          this.setState({ data: data });
          this.setState({ loading: false });
        } else {
          this.setState({ error: data.data });
        }
      });
  }
  noData(msg) {
    this.setState({ noData: true, msg: msg });
  }
  render() {
    return (
      <>
        <h1 style={{ marginBottom: "50px" }}>{this.props.title}</h1>
        {this.state.error ? (
          this.state.error
        ) : this.state.loading ? (
          <Alert variant={"secondary"}>Loading...</Alert>
        ) : (
          <>
            {this.state.noData ? (
              <Alert variant={"secondary"}>
                {this.state.msg ? this.state.msg : "No data!"}
              </Alert>
            ) : (
              this.props.render(
                this.state.data,
                this.noData,
                this.setState,
                this.state
              )
            )}
          </>
        )}
      </>
    );
  }
}

export default CustomList;
