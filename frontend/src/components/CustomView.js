import * as React from "react";
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  useLocation,
  useParams,
  useHistory,
  withRouter,
} from "react-router-dom";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Alert from "react-bootstrap/Alert";
import Table from "react-bootstrap/Table";

class CustomView extends React.Component {
  constructor(props) {
    super(props);
    console.log(this.props);
    this.state = {
      id: this.props.match.params.id,
      loading: true,
      error: null,
      data: null,
    };
  }
  componentDidMount() {
    this.props
      .auth()
      .makeReq(
        true,
        `${this.props.url}${this.state.id ? "/" + this.state.id : ""}`,
        "GET"
      )
      .then((data) => {
        if (data.status === 200) {
          console.log(data);
          this.setState({ data: data });
          this.setState({ loading: false });
        } else {
          this.setState({ error: data.data });
        }
      })
      .catch((err) => {
        this.setState({ error: err });
      });
  }
  componentWillUnmount() {
    console.log("cwu");
  }
  render() {
    const titleStyleOverride = this.props.centerTitle ? "text-center" : null;
    return (
      <>
        <h1 className={titleStyleOverride} style={{ marginBottom: "50px" }}>
          {this.props.title}
        </h1>
        {this.state.error ? (
          JSON.stringify(this.state.error)
        ) : this.state.loading ? (
          <Alert variant={"secondary"}>Loading...</Alert>
        ) : (
          <>{this.props.render(this.state.data)}</>
        )}
      </>
    );
  }
}

export default withRouter(CustomView);
