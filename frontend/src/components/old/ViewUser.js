import * as React from "react";
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  useLocation,
  useParams,
  useHistory,
  withRouter,
} from "react-router-dom";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Table from "react-bootstrap/Table";
import Alert from "react-bootstrap/Alert";

class ViewUser extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: this.props.match.params.userId,
      loading: true,
      error: null,
      user: null,
    };
    console.log("ViewUser constructor");
  }
  componentDidMount() {
    console.log("cdm");
    this.props.auth
      .makeReq(
        true,
        `http://ntukfc.me/api/account/staff/accounts/${this.state.userId}`,
        "GET"
      )
      .then((user) => {
        if (user.status === 200) {
          console.log(user);
          this.setState({ user: user.user });
          this.setState({ loading: false });
          console.log(this.state);
        } else {
          this.setState({ error: user.data });
        }
      })
      .catch((err) => {
        this.setState({ error: err });
      });
  }
  componentWillUnmount() {
    console.log("cwu");
  }
  render() {
    return (
      <>
        <h1>User</h1>
        {this.state.error ? (
          this.state.error
        ) : this.state.loading ? (
          <Alert variant={"secondary"}>Loading...</Alert>
        ) : (
          <>
            <b>ID: </b> <p>{this.state.user._id}</p>
            <b>First Name: </b> <p>{this.state.user.first_name}</p>
            <b>Last Name: </b> <p>{this.state.user.last_name}</p>
            <b>Email: </b> <p>{this.state.user.email}</p>
            <b>Phone Number: </b> <p>{this.state.user.phone_no}</p>
            <b>NHS Number: </b> <p>{this.state.user.nhs_number}</p>
            <b>Last Blood Test: </b> <p>{this.state.user.last_blood_test}</p>
            <b>Groups: </b>{" "}
            <p>
              {this.state.user.groups.map((group) => {
                return group;
              })}
            </p>
          </>
        )}
      </>
    );
  }
}

export default withRouter(ViewUser);
