import * as React from "react";
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  useLocation,
  useHistory,
  useParams,
  useRouteMatch,
} from "react-router-dom";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Nav from "react-bootstrap/Nav";
import Alert from "react-bootstrap/Alert";
import Navbar from "react-bootstrap/Navbar";
import Table from "react-bootstrap/Table";
import NavDropdown from "react-bootstrap/NavDropdown";
import Form from "react-bootstrap/Form";
import FormControl from "react-bootstrap/FormControl";
import ListGroup from "react-bootstrap/ListGroup";
import jwt from "jsonwebtoken";
import { Switch } from "react-router-dom";
import { render } from "react-dom";

class ListUsers extends React.Component {
  constructor(props) {
    super(props);
    this.state = { loading: true, error: null, users: [] };
  }
  componentDidMount() {
    this.props.auth
      .makeReq(true, "http://ntukfc.me/api/account/staff/accounts", "GET")
      .then((users) => {
        if (users.status === 200) {
          this.setState({ users: users.users });
          this.setState({ loading: false });
          console.log(this.state);
        } else {
          this.setState({ error: users.data });
        }
      });
  }
  render() {
    return (
      <>
        <h1>Users</h1>
        {this.state.error ? (
          this.state.error
        ) : this.state.loading ? (
          <Alert variant={"secondary"}>Loading...</Alert>
        ) : (
          <>
            {this.state.users.length > 1 ? (
              <Table>
                <thead>
                  <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Phone</th>
                    <th>View</th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.users.map((user) => {
                    return (
                      <tr key={user._id}>
                        <td>{user.first_name}</td>
                        <td>{user.last_name}</td>
                        <td>{user.phone_no}</td>
                        <td>
                          <Link to={"/usermanagement/view/" + user.email}>
                            View
                          </Link>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </Table>
            ) : (
              <Alert variant={"secondary"}>No Users!</Alert>
            )}
          </>
        )}
      </>
    );
  }
}
export default ListUsers;
