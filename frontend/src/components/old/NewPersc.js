import * as React from "react";
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  useLocation,
  useHistory,
} from "react-router-dom";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Dropdown from "react-bootstrap/Dropdown";
import FormControl from "react-bootstrap/FormControl";

import Alert from "react-bootstrap/Alert";
const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
  <a
    href=""
    ref={ref}
    onClick={(e) => {
      e.preventDefault();
      onClick(e);
    }}
  >
    {children}
    &#x25bc;
  </a>
));

const CustomMenu = React.forwardRef(
  ({ children, style, className, "aria-labelledby": labeledBy }, ref) => {
    const [value, setValue] = React.useState("");
    return (
      <div
        ref={ref}
        style={style}
        className={className}
        aria-labelledby={labeledBy}
      >
        <FormControl
          autoFocus
          className="mx-3 my-2 w-auto"
          placeholder="Type to filter..."
          onChange={(e) => setValue(e.target.value)}
          value={value}
        />
        <ul className="list-unstyled">
          {React.Children.toArray(children.props.children).filter(
            (child) =>
              !value || child.props.children.toLowerCase().startsWith(value)
          )}
        </ul>
      </div>
    );
  }
);

function Meds(get, Placeholder) {
  let [refresh, setRefresh] = React.useState(true);
  let [meds, setMeds] = React.useState([]);
  if (refresh) {
    get().then((meds) => setMeds(meds));
    setRefresh(false);
  }

  if (meds == []) {
    return <Placeholder />;
  } else {
    return (
      <>
        {meds.map((el) => (
          <Dropdown.Item key={el._id} eventKey={JSON.stringify(el)}>
            {el.med_name}
          </Dropdown.Item>
        ))}
      </>
    );
  }
}

function NewPersc({ auth }) {
  const [email, setEmail] = React.useState();
  const [med, setMed] = React.useState({ _id: -1, med_name: "Select..." });
  let [error, setError] = React.useState();
  let [info, setInfo] = React.useState();
  let [mainMsg, setMainMsg] = React.useState();
  const getMeds = () => {
    return new Promise((resolve, reject) => {
      var myHeaders = new Headers();
      myHeaders.append("auth-token", auth.token);
      myHeaders.append("Content-Type", "application/json");

      var requestOptions = {
        method: "GET",
        headers: myHeaders,
        redirect: "follow",
      };

      fetch("http://ntukfc.me/api/medication/", requestOptions)
        .then((response) => response.text())
        .then((result) => {
          console.log(result);
          resolve(JSON.parse(result).medications);
        })
        .catch((error) => reject(error));
    });
  };
  const submitForm = () => {
    var myHeaders = new Headers();
    myHeaders.append("auth-token", auth.token);
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      patient_email: email,
      medication_id: med._id,
    });
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch("http://ntukfc.me/api/perscription/new", requestOptions)
      .then((response) => response.text())
      .then((result) => {
        result = JSON.parse(result);
        setMainMsg(result.message);
      })
      .catch((error) => setError(error));
  };
  return (
    <>
      {mainMsg ? (
        <>
          <Alert variant={info}>{mainMsg}</Alert>
          <Link to="/persc/">Go back</Link>
        </>
      ) : (
        <>
          <h1>New Perscription</h1>
          {info ? <Alert variant="secondary">{info}</Alert> : null}
          {error ? <Alert variant="danger">{error}</Alert> : null}
          <Form>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>patient email</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                onChange={(e) => {
                  setEmail(e.target.value);
                }}
              />
            </Form.Group>
            <Dropdown
              onSelect={(e) => {
                setMed(JSON.parse(e));
              }}
            >
              <Dropdown.Toggle
                as={CustomToggle}
                id="dropdown-custom-components"
              >
                {med.med_name}
              </Dropdown.Toggle>

              <Dropdown.Menu as={CustomMenu}>
                {Meds(getMeds, <p>loading...</p>)}
              </Dropdown.Menu>
            </Dropdown>
            <Button variant="primary" onClick={submitForm}>
              Submit
            </Button>
          </Form>
        </>
      )}
    </>
  );
}

export default NewPersc;
