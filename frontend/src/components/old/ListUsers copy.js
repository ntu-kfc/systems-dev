import * as React from "react";
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  useLocation,
  useHistory,
  useParams,
  useRouteMatch,
} from "react-router-dom";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Nav from "react-bootstrap/Nav";
import Alert from "react-bootstrap/Alert";
import Navbar from "react-bootstrap/Navbar";
import NavDropdown from "react-bootstrap/NavDropdown";
import Form from "react-bootstrap/Form";
import FormControl from "react-bootstrap/FormControl";
import ListGroup from "react-bootstrap/ListGroup";
import jwt from "jsonwebtoken";
import { Switch } from "react-router-dom";
import { render } from "react-dom";

function ListUsers({ auth }) {
  let [users, setUsers] = React.useState([]);
  let [refresh, setRefresh] = React.useState(true);
  let [error, setError] = React.useState();
  let [info, setInfo] = React.useState();
  const getReqs = () => {
    return new Promise((resolve, reject) => {
      var myHeaders = new Headers();
      myHeaders.append("auth-token", auth.token);
      myHeaders.append("Content-Type", "application/json");

      var requestOptions = {
        method: "GET",
        headers: myHeaders,
        redirect: "follow",
      };

      fetch("http://ntukfc.me/api/account/staff/accounts", requestOptions)
        .then((response) => response.text())
        .then((result) => {
          result = JSON.parse(result).users;
          result = result.map((usr) => {
            usr.hidden = true;
            return usr;
          });
          resolve(result);
        })
        .catch((error) => reject(error));
    });
  };

  if (refresh) {
    setRefresh(false);
    setInfo("Loading");
    getReqs().then((usrs) => {
      setUsers(usrs);
      if (usrs.length == 0) {
        setInfo("No requests");
      } else {
        setInfo(null);
      }
    });
  }
  return (
    <>
      <h1>Users</h1>
      {info ? <Alert variant="secondary">{info}</Alert> : null}
      <ListGroup variant="flush">
        {users ? (
          users.map((usr) => (
            <ListGroup.Item key={usr._id}>
              {usr.email}
              {console.log(usr)}
              <Link to={"/usermanagement/view/" + usr.email}>View</Link>
              {usr.hidden ? null : (
                <ListGroup>
                  <ListGroup.Item>{usr.nhs_number}</ListGroup.Item>
                </ListGroup>
              )}

              <Button
                onClick={() => {
                  usr.hidden = !usr.hidden;
                }}
              >
                Show
              </Button>
            </ListGroup.Item>
          ))
        ) : (
          <h1>loading</h1>
        )}
      </ListGroup>
    </>
  );
}
export default ListUsers;
