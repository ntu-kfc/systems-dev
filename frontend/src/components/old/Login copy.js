import * as React from "react";
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  useLocation,
  useHistory,
} from "react-router-dom";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";

function Login(props) {
  const auth = props.auth;
  const [redirectToReferrer, setRedirectToReferrer] = React.useState(false);

  const [uname, setLogin] = React.useState();
  const [pass, setPass] = React.useState();
  const { state } = useLocation();

  if (auth.isAuthenticated) return <Redirect to="/home" />;
  const login = () =>
    auth.getToken(
      () => {
        setRedirectToReferrer(true);
      },
      uname,
      pass
    );

  if (redirectToReferrer === true) {
    return <Redirect to={state?.from || "/"} />;
  }

  return (
    <>
      <Form
        onSubmit={(e) => {
          e.preventDefault();
          login();
        }}
      >
        <fieldset>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email"
              onChange={(e) => {
                setLogin(e.target.value);
              }}
            />
            <Form.Text className="text-muted">
              We'll never share your email with anyone else.
            </Form.Text>
          </Form.Group>

          <Form.Group controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              onChange={(e) => {
                console.log(e);
                setPass(e.target.value);
              }}
            />
          </Form.Group>
          <Button variant="primary" type="submit">
            Submit
          </Button>
        </fieldset>
      </Form>
      <Button href="/request">request access</Button>
    </>
  );
}
export default Login;
