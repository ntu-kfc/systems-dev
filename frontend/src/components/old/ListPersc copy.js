import * as React from "react";
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  useLocation,
  useHistory,
} from "react-router-dom";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Alert from "react-bootstrap/Alert";
import ListGroup from "react-bootstrap/ListGroup";

function ListPersc({ auth }) {
  let [perscs, setPerscs] = React.useState([]);
  let [refresh, setRefresh] = React.useState(true);
  let [error, setError] = React.useState();
  let [info, setInfo] = React.useState();
  const getPersc = () => {
    return new Promise((resolve, reject) => {
      var myHeaders = new Headers();
      myHeaders.append("auth-token", auth.token);
      myHeaders.append("Content-Type", "application/json");

      var requestOptions = {
        method: "GET",
        headers: myHeaders,
        redirect: "follow",
      };

      fetch("http://ntukfc.me/api/perscription", requestOptions)
        .then((response) => response.text())
        .then((result) => resolve(JSON.parse(result).perscriptions))
        .catch((error) => reject(error));
    });
  };

  if (refresh) {
    setRefresh(false);
    setInfo("Loading");
    getPersc().then((perscs) => {
      setPerscs(perscs);
      if (perscs.length == 0) {
        setInfo("No requests");
      } else {
        setInfo(null);
      }
    });
  }
  return (
    <>
      <h1>Perscriptions</h1>
      {info ? <Alert variant="secondary">{info}</Alert> : null}
      <ListGroup variant="flush">
        {perscs.map((persc) => (
          <ListGroup.Item key={persc._id}>
            {persc.patient_email} |{" "}
            {persc.medication_data.map((el) => el.med_name)}
            <Link to={"/persc/view/" + persc._id}> View </Link>
          </ListGroup.Item>
        ))}
      </ListGroup>
    </>
  );
}

export default ListPersc;
