import * as React from "react";
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  useLocation,
  useParams,
  useHistory,
} from "react-router-dom";
import Button from "react-bootstrap/Button";
import Alert from "react-bootstrap/Alert";
import Dropdown from "react-bootstrap/Dropdown";
import FormControl from "react-bootstrap/FormControl";
import Form from "react-bootstrap/Form";
import { FormikContext, useFormik } from "formik";
import { useEffect } from "react";
import ReactDOM from "react-dom";
import { Formik, Form as FormikForm, useField, useFormikContext } from "formik";
import * as Yup from "yup";
import styled from "@emotion/styled";

const MyTextInput = ({ label, ...props }) => {
  // useField() returns [formik.getFieldProps(), formik.getFieldMeta()]
  // which we can spread on <input> and alse replace ErrorMessage entirely.
  const [field, meta] = useField(props);
  return (
    <>
      <Form.Label htmlFor={props.id || props.name} style={{ fontSize: 30 }}>
        {label}
      </Form.Label>
      {meta.touched && meta.error ? (
        <Alert variant={"danger"}>{meta.error}</Alert>
      ) : null}
      <Form.Control className="text-input" {...field} {...props} />
    </>
  );
};

// And now we can use these
const SignupForm = () => {
  const phoneRegEx = /^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$/;
  const nhsNrRegEx = /^\d{3}\s?\-?\d{3}\s?\-?\d{4}$/;
  const [sub, setSub] = React.useState(null);
  return (
    <>
      {sub ? (
        <>{sub}</>
      ) : (
        <Formik
          initialValues={{
            first_name: "",
            last_name: "",
            email: "",
            phone_no: "",
            nhs_number: "",
            password: "",
            reason: "",
          }}
          validationSchema={Yup.object({
            first_name: Yup.string()
              .max(15, "Must be 15 characters or less")
              .required("Required"),
            last_name: Yup.string()
              .max(20, "Must be 20 characters or less")
              .required("Required"),
            email: Yup.string()
              .email("Invalid email addresss`")
              .required("Required"),
            phone_no: Yup.string()
              .matches(phoneRegEx, "Invalid phone number")
              .required("Required"),
            password: Yup.string().required("Required"),
            nhs_number: Yup.string()
              .matches(nhsNrRegEx, "Invalid NHS number")
              .required("Required"),
            reason: Yup.string().required("Required"),
          })}
          onSubmit={async (values, { setSubmitting }) => {
            var myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");

            var raw = JSON.stringify(values);

            var requestOptions = {
              method: "POST",
              headers: myHeaders,
              body: raw,
              redirect: "follow",
            };

            fetch("http://ntukfc.me/api/auth/request_access", requestOptions)
              .then((response) => response.text())
              .then((result) => {
                result = JSON.parse(result);
                setSub(
                  <Alert variant="success">
                    <Alert.Heading>Success</Alert.Heading>
                    <p>{result["message"]}</p>
                  </Alert>
                );
                setSubmitting(false);
              })
              .catch((error) => console.log("error", error));
          }}
        >
          {({ errors, touched, isSubmitting }) => (
            <FormikForm>
              <MyTextInput
                label="First Name"
                name="first_name"
                type="text"
                placeholder="Jane"
              />
              <MyTextInput
                label="Last Name"
                name="last_name"
                type="text"
                placeholder="Doe"
              />
              <MyTextInput
                label="Email Address"
                name="email"
                type="email"
                placeholder="jane@email.com"
              />
              <MyTextInput
                label="Password"
                name="password"
                type="password"
                placeholder=""
              />
              <MyTextInput
                label="Phone number"
                name="phone_no"
                type="text"
                placeholder="07900000000"
              />
              <MyTextInput
                label="NHS Number"
                name="nhs_number"
                type="text"
                placeholder="000-000-0000"
              />
              <MyTextInput label="Reason" name="reason" type="text" />

              <Button type="submit" disabled={isSubmitting}>
                Submit
              </Button>
            </FormikForm>
          )}
        </Formik>
      )}
    </>
  );
};

function Request() {
  return <SignupForm />;
}

export default Request;
