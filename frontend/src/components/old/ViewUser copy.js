import * as React from "react";
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  useLocation,
  useParams,
  useHistory,
} from "react-router-dom";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Table from "react-bootstrap/Table";

function User({ get }) {
  let [refresh, setRefresh] = React.useState(true);
  let [user, setUser] = React.useState();

  if (refresh) {
    get().then((ret) => setUser(ret.user));
    setRefresh(false);
  }

  if (!user) {
    return <h1>loading</h1>;
  } else {
    return (
      <>
        <h2>ID: {user._id}</h2>
        <h2>
          Name: {user.first_name} {user.last_name}
        </h2>
        <h2>Email: {user.email}</h2>
        <h2>Phone: {user.phone_no}</h2>
        <h2>NHS Number: {user.nhs_number}</h2>
        <h2>Groups: {user.groups.map((group) => group)}</h2>
      </>
    );
  }
}

function ViewRequest({ auth }) {
  let { reqId } = useParams();
  let [error, setError] = React.useState();
  let [info, setInfo] = React.useState();
  const getReq = () => {
    return new Promise((resolve, reject) => {
      var myHeaders = new Headers();
      myHeaders.append("auth-token", auth.token);
      myHeaders.append("Content-Type", "application/json");

      var requestOptions = {
        method: "GET",
        headers: myHeaders,
        redirect: "follow",
      };

      fetch(
        `http://ntukfc.me/api/account/staff/accounts/${reqId}`,
        requestOptions
      )
        .then((response) => response.text())
        .then((result) => resolve(JSON.parse(result)))
        .catch((error) => reject(error));
    });
  };
  return <User get={getReq} />;
}

export default ViewRequest;
