import * as React from "react";
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  useLocation,
  useParams,
  useHistory,
} from "react-router-dom";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";

function Persc({ get }) {
  let [refresh, setRefresh] = React.useState(true);
  let [persc, setPersc] = React.useState();

  if (refresh) {
    get().then((persc) => setPersc(persc));
    setRefresh(false);
  }

  if (!persc) {
    return <h1>loading</h1>;
  } else {
    return <h1>{JSON.stringify(persc)}</h1>;
  }
}

function ViewPersc({ auth }) {
  let { perscId } = useParams();
  let [error, setError] = React.useState();
  let [info, setInfo] = React.useState();
  const getPersc = () => {
    return new Promise((resolve, reject) => {
      var myHeaders = new Headers();
      myHeaders.append("auth-token", auth.token);
      myHeaders.append("Content-Type", "application/json");

      var requestOptions = {
        method: "GET",
        headers: myHeaders,
        redirect: "follow",
      };

      fetch(`http://ntukfc.me/api/perscription/${perscId}`, requestOptions)
        .then((response) => response.text())
        .then((result) => resolve(JSON.parse(result)))
        .catch((error) => reject(error));
    });
  };
  return <Persc get={getPersc} />;
}

export default ViewPersc;
