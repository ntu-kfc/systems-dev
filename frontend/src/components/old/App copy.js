import * as React from "react";
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  useLocation,
  useHistory,
  useRouteMatch,
} from "react-router-dom";
import Button from "react-bootstrap/Button";
import Login from "./components/Login";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import NavDropdown from "react-bootstrap/NavDropdown";
import Form from "react-bootstrap/Form";
import FormControl from "react-bootstrap/FormControl";
import jwt from "jsonwebtoken";
import { Switch } from "react-router-dom";
import { render } from "react-dom";
import NewPersc from "./components/NewPersc";
import MedsList from "./components/MedsList";
import ViewUser from "./components/ViewUser";
import ListPersc from "./components/ListPersc";
import Request from "./components/Request";
import ViewPersc from "./components/ViewPersc";
import ListRequests from "./components/ListRequests";
import ViewRequest from "./components/ViewRequest";
import ListUsers from "./components/ListUsers";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHome } from "@fortawesome/free-solid-svg-icons";
const settings = {
  nav: {
    showIcons: true,
    showName: true,
  },
};

const auth = {
  isAuthenticated: false,
  token: "",
  perms: [],
  user: {},
  loginCbs: [],
  callLoginCbs() {
    this.loginCbs.map((fun) => fun());
  },
  getToken(cb, uname, pass) {
    var loginHead = new Headers();
    loginHead.append("Content-Type", "application/json");

    var raw = JSON.stringify({ email: uname, password: pass });

    var loginOpts = {
      method: "POST",
      headers: loginHead,
      body: raw,
      redirect: "follow",
    };

    fetch("http://ntukfc.me/api/auth/login", loginOpts)
      .then((loginResp) => loginResp.text())
      .then((loginResult) => JSON.parse(loginResult))
      .then((loginResult) => {
        if (loginResult.access_token) {
          this.token = loginResult.access_token;
          this.checkToken();
          this.callLoginCbs();
        }
      })
      .catch((error) => console.log("error", error));
  },
  checkToken() {
    if (!this.token) return;
    try {
      var userHead = new Headers();
      userHead.append("auth-token", this.token);
      userHead.append("Content-Type", "application/json");
      var userOpts = {
        method: "GET",
        headers: userHead,
        redirect: "follow",
      };

      fetch("http://ntukfc.me/api/account", userOpts)
        .then((response) => response.text())
        .then((result) => JSON.parse(result))
        .then((result) => {
          this.user = result.user;
          localStorage.setItem("token", this.token);
          this.checkPerms();
        });

      this.isAuthenticated = true;
    } catch (err) {
      console.log(err);
      this.signout();
    }
  },
  checkPerms() {
    this.makeReqAuth("http://ntukfc.me/api/groups/").then((groupsInfo) => {
      groupsInfo = groupsInfo.groups;
      this.user.groups.forEach((u) => {
        groupsInfo.forEach((g) => {
          if (u == g.group_name) {
            this.perms = this.perms.concat(g.group_perms);
          }
        });
      });
    });
  },
  makeReq(url) {
    return new Promise((resolve, reject) => {
      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");

      var requestOptions = {
        method: "GET",
        headers: myHeaders,
        redirect: "follow",
      };

      fetch(url, requestOptions)
        .then((response) => response.text())
        .then((result) => {
          resolve(JSON.parse(result));
        })
        .catch((error) => reject(error));
    });
  },
  makeReqAuth(url) {
    return new Promise((resolve, reject) => {
      if (!this.isAuthenticated) {
        reject("Not authenticated");
      }

      var myHeaders = new Headers();
      myHeaders.append("auth-token", this.token);
      myHeaders.append("Content-Type", "application/json");

      var requestOptions = {
        method: "GET",
        headers: myHeaders,
        redirect: "follow",
      };

      fetch(url, requestOptions)
        .then((response) => response.text())
        .then((result) => {
          resolve(JSON.parse(result));
        })
        .catch((error) => reject(error));
    });
  },
  signout(cb) {
    this.isAuthenticated = false;
    this.token = "";
    this.user = {};
    localStorage.removeItem("token");
    window.location.reload();
  },
};

function RouteWrapper({ children, priv, ...rest }) {
  if (rest.hideInSubs && rest.path != window.location.pathname) {
    children = children[1];
  }

  return (
    <Route
      {...rest}
      render={({ location }) => {
        if (priv) {
          if (auth.isAuthenticated) {
            return children;
          } else {
            return (
              <Redirect
                to={{
                  pathname: "/login",
                  state: { from: location },
                }}
              />
            );
          }
        }
        return children;
      }}
    />
  );
}

let routes = [
  {
    name: "Home",
    url: "/home",
    private: true,
    comp: <h1>home</h1>,
    icon: <FontAwesomeIcon icon={faHome} size="4x" />,
  },
  {
    name: "Perscriptions",
    url: "/persc",
    private: true,
    hidden: true,
    hideInSubs: true,
    comp: <Redirect from="/percs" to="/persc/list" />,
    subs: [
      {
        name: "List Perscriptions",
        url: "/list",
        perms: ["view.perscription.*"],
        private: true,
        comp: <ListPersc auth={auth} />,
      },
      {
        name: "New Perscriptions",
        url: "/new",
        private: true,
        perms: ["perscription.create"],
        comp: <NewPersc auth={auth} />,
      },
      {
        name: "View",
        url: "/view/:perscId",
        private: true,
        hidden: true,
        perms: ["view.perscription.*"],
        comp: <ViewPersc auth={auth} />,
      },
    ],
  },
  {
    name: "Manage Requests",
    url: "/managerequests",
    perms: ["account_requests.grant", "account_requests.deny"],
    hideInSubs: true,
    private: true,
    comp: <ListRequests auth={auth} />,
    subs: [
      {
        name: "View Request",
        url: "/view/:reqId",
        perms: ["account_requests.grant", "account_requests.deny"],
        hidden: true,
        comp: <ViewRequest auth={auth} />,
      },
    ],
  },
  {
    name: "User Management",
    url: "/usermanagement",
    private: true,
    comp: <ListUsers auth={auth} />,
    hideInSubs: true,
    perms: ["view.patient.*"],
    subs: [
      {
        name: "View Request",
        url: "/view/:reqId",
        hidden: true,
        comp: <ViewUser auth={auth} />,
      },
    ],
  },
  {
    name: "request",
    url: "/request",
    hidden: true,
    comp: <Request />,
  },
  {
    name: "login",
    url: "/login",
    hidden: true,
    private: false,
    perms: ["user.login"],
    comp: <Login auth={auth} />,
  },
  {
    name: "Logout",
    url: "/logout",
    perms: ["user.logout"],
    onClick: () => auth.signout(),
    comp: <></>,
  },
  {
    name: "homeredirect",
    url: "",
    comp: <Redirect exact to="/home" />,
    hidden: true,
  },
];

function renderRoutes(routes, path, hideParent) {
  if (!path) path = "";
  hideParent = typeof hideParent !== "undefined" ? hideParent : false;
  if (!routes) return;
  return (
    <>
      {
        <Switch>
          {routes.map((route) => {
            return (
              <RouteWrapper
                priv={route.private ? route.private : false}
                key={path + route.url}
                path={path + route.url}
                hideInSubs={route.hideInSubs}
              >
                {route.comp}
                {route.subs && route.subs.length > 0 ? (
                  renderRoutes(route.subs, path + route.url, route.hideInSubs)
                ) : (
                  <></>
                )}
              </RouteWrapper>
            );
          })}
        </Switch>
      }
    </>
  );
}

function makeNav(routes, path, depth, cb) {
  if (!depth) depth = 0;
  if (!path) path = "";
  if (!routes) {
    return;
  }
  return (
    <>
      {routes.map((el) => {
        let badperms = false;
        if (el.perms) {
          el.perms.map((perm) => {
            if (auth.perms.indexOf(perm) === -1) {
              badperms = true;
            }
          });
        }
        if (!el.hidden) {
          if (!badperms || auth.perms.includes("*")) {
            return (
              <div key={path + el.url}>
                <Link
                  to={path + el.url}
                  key={"Link:" + path + el.url}
                  onClick={el.onClick ? el.onClick : null}
                >
                  <div
                    style={{
                      display: "list-item",
                      alignContent: "center",
                      textAlign: "center",
                      textDecoration: "none",
                      listStyle: "none",
                    }}
                  >
                    {settings.nav.showIcons ? el.icon : null}
                    {settings.nav.showName ? (
                      <p>{el.name}</p>
                    ) : el.icon ? null : ( // fallback to text if has no icon and set to only show icons
                      <p>{el.name}</p>
                    )}
                  </div>
                </Link>
                {el.subs && el.subs.length > 0 ? (
                  makeNav(el.subs, path + el.url, Number(depth) + 1)
                ) : (
                  <></>
                )}
              </div>
            );
          }
        }
        if (el.subs && el.subs.length > 0) {
          return <>{makeNav(el.subs, path + el.url, Number(depth) + 1)}</>;
        }
      })}
    </>
  );
}

function CustomNav(props) {
  return makeNav(props.routes, props.path);
}

class App extends React.Component {
  constructor(props) {
    super(props);
    let token = localStorage.getItem("token");
    this.state = { auth: auth };
    if (token) {
      auth.token = token;
      auth.checkToken();
    }
    auth.loginCbs.push(() => {
      this.setState({ auth: auth });
    });
  }
  render() {
    return (
      <>
        <Container fluid>
          <Router>
            <Row>
              {this.state.auth.isAuthenticated ? (
                <>
                  <Col xs={2}>
                    <CustomNav routes={routes} path="" depth="0" />
                  </Col>
                </>
              ) : (
                <></>
              )}
              <Col>{renderRoutes(routes, "")}</Col>
            </Row>
          </Router>
        </Container>
      </>
    );
  }
}

export default App;
