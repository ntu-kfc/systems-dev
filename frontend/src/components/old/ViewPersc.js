import * as React from "react";
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  useLocation,
  useParams,
  useHistory,
  withRouter,
} from "react-router-dom";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Alert from "react-bootstrap/Alert";
import Table from "react-bootstrap/Table";

class ViewPersc extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      perscId: this.props.match.params.perscId,
      loading: true,
      error: null,
      persc: null,
    };
  }
  componentDidMount() {
    console.log("cdm");
    this.props.auth
      .makeReq(
        true,
        `http://ntukfc.me/api/perscription/${this.state.perscId}`,
        "GET"
      )
      .then((persc) => {
        if (persc.status === 200) {
          console.log(persc);
          this.setState({ persc: persc.perscription });
          this.setState({ loading: false });
        } else {
          this.setState({ error: persc.data });
        }
      })
      .catch((err) => {
        this.setState({ error: err });
      });
  }
  componentWillUnmount() {
    console.log("cwu");
  }
  render() {
    return (
      <>
        <h1>Perscription</h1>
        {this.state.error ? (
          this.state.error
        ) : this.state.loading ? (
          <Alert variant={"secondary"}>Loading...</Alert>
        ) : (
          <>
            <b>Perscription ID: </b> <p>{this.state.persc._id}</p>
            <b>Patient Email: </b> <p>{this.state.persc.patient_email}</p>
            <b>Created At: </b> <p>{this.state.persc.created_at}</p>
            <b>Collection Time: </b> <p>{this.state.persc.collection_time}</p>
            <b>Bloodtest Needed: </b>{" "}
            <p>{this.state.persc.bloodtest_needed ? "Yes" : "No"}</p>
            <b>Bloodtest Completed: </b>{" "}
            <p>{this.state.persc.bloodtest_completed ? "Yes" : "No"}</p>
            <b>Medication Name: </b>{" "}
            <p>{this.state.persc.medication_data[0].med_name}</p>
            <b>Days Without Blood Test: </b>{" "}
            <p>
              {this.state.persc.medication_data[0].med_blood_work_days_without
                ? this.state.persc.medication_data[0]
                    .med_blood_work_days_without
                : "No Data"}
            </p>
          </>
        )}
      </>
    );
  }
}

export default withRouter(ViewPersc);
