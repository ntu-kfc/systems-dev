import * as React from "react";
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  useLocation,
  useHistory,
} from "react-router-dom";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Alert from "react-bootstrap/Alert";
import Table from "react-bootstrap/Table";
import ListGroup from "react-bootstrap/ListGroup";

class CustomList extends React.Component {
  constructor(props) {
    super(props);
    this.state = { loading: true, error: null, data: [] };
  }
  componentDidMount() {
    this.props.auth
      .makeReq(true, this.props.url, this.props.method)
      .then((data) => {
        if (data.status === 200) {
          this.setState({ data: data });
          this.setState({ loading: false });
        } else {
          this.setState({ error: data.data });
        }
      });
  }
  render() {
    return (
      <>
        <h1>{this.props.title}</h1>
        {this.state.error ? (
          this.state.error
        ) : this.state.loading ? (
          <Alert variant={"secondary"}>Loading...</Alert>
        ) : (
          <>{this.props.render(this.state.data)}</>
        )}
      </>
    );
  }
}

class ListPersc extends React.Component {
  constructor(props) {
    super(props);
    this.state = { loading: true, error: null, perscriptions: [] };
  }
  componentDidMount() {
    this.props.auth
      .makeReq(true, "http://ntukfc.me/api/perscription", "GET")
      .then((perscs) => {
        if (perscs.status === 200) {
          this.setState({ perscriptions: perscs.perscriptions });
          this.setState({ loading: false });
          console.log(this.state);
        } else {
          this.setState({ error: perscs.data });
        }
      });
  }
  render() {
    return (
      <>
        <h1>Perscriptions</h1>
        {this.state.error ? (
          this.state.error
        ) : this.state.loading ? (
          <Alert variant={"secondary"}>Loading...</Alert>
        ) : (
          <>
            {this.state.perscriptions.length > 1 ? (
              <Table>
                <thead>
                  <tr>
                    <th>Patient Email</th>
                    <th>Collected</th>
                    <th>Collection Date</th>
                    <th>View</th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.perscriptions.map((persc) => {
                    let colDate = new Date(persc.collection_time);
                    return (
                      <tr key={persc._id}>
                        <td>{persc.patient_email}</td>
                        <td>{persc.collected ? "Yes" : "No"}</td>
                        <td>{`${
                          colDate.getDate() / colDate.getMonth()
                        }/${colDate.getFullYear()} @ ${colDate.getHours()}:${colDate.getMinutes()}`}</td>
                        <td>
                          <Link to={"/persc/view/" + persc._id}>View</Link>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </Table>
            ) : (
              <Alert variant={"secondary"}>No Perscriptions!</Alert>
            )}
          </>
        )}
      </>
    );
  }
}

export default ListPersc;
