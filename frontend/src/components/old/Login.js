import * as React from "react";
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  useLocation,
  useParams,
  useHistory,
} from "react-router-dom";
import Button from "react-bootstrap/Button";
import Alert from "react-bootstrap/Alert";
import Dropdown from "react-bootstrap/Dropdown";
import FormControl from "react-bootstrap/FormControl";
import Form from "react-bootstrap/Form";
import { FormikContext, useFormik } from "formik";
import { useEffect } from "react";
import ReactDOM from "react-dom";
import { Formik, Form as FormikForm, useField, useFormikContext } from "formik";
import * as Yup from "yup";
import styled from "@emotion/styled";

const MyTextInput = ({ label, ...props }) => {
  const [field, meta] = useField(props);
  return (
    <>
      <Form.Label htmlFor={props.id || props.name} style={{ fontSize: 30 }}>
        {label}
      </Form.Label>
      {meta.touched && meta.error ? (
        <Alert variant={"danger"}>{meta.error}</Alert>
      ) : null}
      <Form.Control className="text-input" {...field} {...props} />
    </>
  );
};

const LoginForm = ({ auth, setAuthed, setLoading }) => {
  let [error, setError] = React.useState(null);
  return (
    <>
      {error ? error : null}
      <Formik
        initialValues={{
          email: "",
          password: "",
        }}
        validationSchema={Yup.object({
          email: Yup.string()
            .email("Invalid email addresss`")
            .required("Required"),
          password: Yup.string().required("Required"),
        })}
        onSubmit={this.props.submit}
      >
        {({ isSubmitting }) => (
          <FormikForm>
            <MyTextInput label="E-mail" name="email" type="email" />
            <MyTextInput label="Password" name="password" type="password" />

            <Button type="submit" disabled={isSubmitting}>
              Submit
            </Button>
          </FormikForm>
        )}
      </Formik>
    </>
  );
};

class Login extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <LoginForm
        auth={this.props.auth}
        setAuthed={this.props.setAuthed}
        setLoading={this.props.setLoading}
      />
    );
  }
}

export default Login;
