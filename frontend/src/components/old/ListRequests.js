import * as React from "react";
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  useLocation,
  useHistory,
  useParams,
  useRouteMatch,
} from "react-router-dom";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Nav from "react-bootstrap/Nav";
import Alert from "react-bootstrap/Alert";
import Navbar from "react-bootstrap/Navbar";
import NavDropdown from "react-bootstrap/NavDropdown";
import Form from "react-bootstrap/Form";
import FormControl from "react-bootstrap/FormControl";
import ListGroup from "react-bootstrap/ListGroup";
import jwt from "jsonwebtoken";
import { Switch } from "react-router-dom";
import { render } from "react-dom";

function ListRequests({ auth, location }) {
  let [reqs, setReqs] = React.useState([]);
  let [refresh, setRefresh] = React.useState(true);
  let [error, setError] = React.useState();
  let [info, setInfo] = React.useState();
  auth = auth();
  const getReqs = () => {
    return new Promise((resolve, reject) => {
      var myHeaders = new Headers();
      myHeaders.append("auth-token", auth.token);
      myHeaders.append("Content-Type", "application/json");

      var requestOptions = {
        method: "GET",
        headers: myHeaders,
        redirect: "follow",
      };

      fetch(
        "http://ntukfc.me/api/account/staff/account_requests",
        requestOptions
      )
        .then((response) => response.text())
        .then((result) => {
          resolve(JSON.parse(result).requests);
        })
        .catch((error) => reject(error));
    });
  };

  if (refresh) {
    setRefresh(false);
    setInfo("Loading");
    getReqs().then((perscs) => {
      setReqs(perscs);
      if (!perscs) {
        setInfo("Error getting requests");
        return;
      }
      if (perscs.length == 0) {
        setInfo("No requests");
      } else {
        setInfo(null);
      }
    });
  }
  return (
    <>
      <h1>Access Requests</h1>
      {info ? <Alert variant="secondary">{info}</Alert> : null}
      <ListGroup variant="flush">
        {reqs ? (
          reqs.map((req) => (
            <ListGroup.Item key={req._id}>
              {req.email}
              <Link to={"/managerequests/view/" + req.email}>View</Link>
            </ListGroup.Item>
          ))
        ) : (
          <h1>loading</h1>
        )}
      </ListGroup>
    </>
  );
}
export default ListRequests;
