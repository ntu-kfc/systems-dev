import * as React from "react";
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  useLocation,
  useParams,
  useHistory,
  withRouter,
} from "react-router-dom";
import Button from "react-bootstrap/Button";
import Alert from "react-bootstrap/Alert";
import Dropdown from "react-bootstrap/Dropdown";
import * as Yup from "yup";
import FormControl from "react-bootstrap/FormControl";
import Form from "react-bootstrap/Form";
import { FormikContext, useFormik } from "formik";
import { useEffect } from "react";
import Col from "react-bootstrap/Col";
import ReactDOM from "react-dom";
import { Formik, Form as FormikForm, useField, useFormikContext } from "formik";
import styled from "@emotion/styled";

class CustomForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { msg: null };
  }
  render() {
    return (
      <>
        <Formik
          initialValues={
            this.props.initial
              ? this.props.initial
              : this.props.location.state
              ? this.props.location.state.prefil
              : ""
          }
          validationSchema={this.props.schema}
          onSubmit={(values, { ...all }) => {
            console.log("xxxxxxxxxxxxxxx");
            console.log(all);
            this.props.submit(values, this.props, all);
          }}
        >
          {(bag) => {
            if (bag.status) {
              return (
                <>
                  <Col md={{ span: 4, offset: 4 }}>
                    <Alert
                      variant={
                        Number(bag.status.status) == 200 ? "success" : "danger"
                      }
                    >
                      {bag.status.message
                        ? bag.status.message
                        : bag.status.error_description
                        ? bag.status.error_description
                        : bag.status.error}
                      <hr />
                      <Button onClick={this.props.history.goBack}>
                        Go Back
                      </Button>
                    </Alert>
                  </Col>
                </>
              );
            } else {
              const setState = this.setState.bind(this);
              const titleStyleOverride = this.props.centerTitle
                ? "text-center"
                : null;
              return (
                <>
                  <h1
                    className={titleStyleOverride}
                    style={{ marginBottom: "50px" }}
                  >
                    {this.props.title}
                  </h1>
                  <FormikForm>
                    {this.props.render(bag, this.props, setState)}
                  </FormikForm>
                </>
              );
            }
          }}
        </Formik>
      </>
    );
  }
}

export default withRouter(CustomForm);
