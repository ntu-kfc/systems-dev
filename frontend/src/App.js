import * as React from "react";
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  useLocation,
  useHistory,
  useRouteMatch,
  withRouter,
} from "react-router-dom";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import * as Yup from "yup";
import { Formik, Form as FormikForm, useField, useFormikContext } from "formik";
import Row from "react-bootstrap/Row";
import Nav from "react-bootstrap/Nav";
import Media from "react-bootstrap/Media";
import Navbar from "react-bootstrap/Navbar";
import NavDropdown from "react-bootstrap/NavDropdown";
import Form from "react-bootstrap/Form";
import FormControl from "react-bootstrap/FormControl";
import Dropdown from "react-bootstrap/Dropdown";
import DropdownButton from "react-bootstrap/DropdownButton";
import Table from "react-bootstrap/Table";
import jwt from "jsonwebtoken";
import { Switch } from "react-router-dom";
import Alert from "react-bootstrap/Alert";
import { render } from "react-dom";
import MedsList from "./components/MedsList";
import CustomList from "./components/CustomList";
import CustomView from "./components/CustomView";
import CustomForm from "./components/CustomForm";
import CustomDropdown from "./components/CustomDropdown";
import ViewRequest from "./components/ViewRequest";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHome } from "@fortawesome/free-solid-svg-icons";
import { isTemplateExpression } from "typescript";
const settings = {
  nav: {
    showIcons: true,
    showName: true,
  },
};
const capitalize = (str) => {
  if (typeof str === "string") {
    return str.replace(/^\w/, (c) => c.toUpperCase());
  } else {
    return "";
  }
};
/*
class RouteWrapper extends React.Component {
  constructor(props) {
    super(props);
    let { children, priv, ...rest } = props;
    this.children = children;
    this.priv = priv;
    this.rest = rest;
  }
  render() {
    console.log(`RouteWrapper.render()`);
    console.log("Props:");
    console.log(this.props);
    console.log(this.rest);
    console.log(this.rest.hideInSubs);
    let test =
      typeof this.rest.hideInSubs == "undefined" ? false : this.rest.hideInSubs;

    return (
      <Route
        {...this.rest}
        render={({ location }) => {
          if (this.priv) {
            if (this.props.auth.isAuthenticated) {
              return this.children;
            } else {
              return (
                <Redirect
                  to={{
                    pathname: "/login",
                    state: { from: location },
                  }}
                />
              );
            }
          }
        }}
      />
    );
  }
}*/

class RouteWrapper extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <>
        <Route
          {...this.props.location}
          {...this.props.computedMatch}
          render={(props2) => {
            return (
              <>
                {this.props.children.map((child) => {
                  console.log(child);
                  if (child.comp) {
                    // is a custom route
                    if (child.revperms) {
                      console.log(this.props.auth().perms);
                      console.log(child.revperms);
                      console.log(
                        this.props.auth().perms.indexOf(child.revperms[0])
                      );
                      child.revperms.forEach((perm) => {
                        if (this.props.auth().perms.indexOf(perm) !== -1) {
                          console.log("ret");
                          return (
                            <Redirect
                              to={{
                                pathname: "/myinfo",
                              }}
                            />
                          );
                        }
                      });
                    }
                    if (child.restrict) {
                      if (child.restrict === "notloggedin") {
                        if (this.props.auth().isAuthenticated) {
                          return (
                            <Redirect
                              to={{
                                pathname: "/myinfo",
                              }}
                            />
                          );
                        }

                        return (
                          <div key={JSON.stringify(child)}>{child.comp}</div>
                        );
                      } else if (child.restrict === "loggedin") {
                        if (this.props.auth().isAuthenticated) {
                          if (child.hideInSubs) {
                            if (props2.match.isExact) {
                              return (
                                <div key={JSON.stringify(child)}>
                                  {child.comp}
                                </div>
                              );
                            } else {
                              return null;
                            }
                          } else {
                            return (
                              <div key={JSON.stringify(child)}>
                                {child.comp}
                              </div>
                            );
                          }
                        } else {
                          return (
                            <Redirect
                              to={{
                                pathname: "/login",
                              }}
                            />
                          );
                        }
                      }
                    } else {
                      return (
                        <div key={JSON.stringify(child)}>{child.comp}</div>
                      );
                    }
                  } else {
                    // is a react object
                    return child;
                  }
                })}
              </>
            );
          }}
        />
      </>
    );
  }
}
class Routes extends React.Component {
  constructor(props) {
    super(props);
  }

  routeBuilder(routes, path, hideParent) {
    path = typeof path !== "undefined" ? path : "";
    hideParent = typeof hideParent !== "undefined" ? hideParent : false;
    routes = typeof routes !== "undefined" ? routes : null;
    if (!routes) return;
    return (
      <>
        {
          <Switch>
            {routes.map((route) => {
              return (
                <RouteWrapper
                  auth={this.props.auth}
                  key={path + route.url}
                  path={path + route.url}
                >
                  {route}
                  {route.subs && route.subs.length > 0 ? (
                    this.routeBuilder(
                      route.subs,
                      path + route.url,
                      route.hideInSubs
                    )
                  ) : (
                    <></>
                  )}
                </RouteWrapper>
              );
            })}
          </Switch>
        }
      </>
    );
  }
  render() {
    return this.routeBuilder(this.props.routes, "");
  }
}

class NavBar extends React.Component {
  constructor(props) {
    super(props);
  }

  navBuilder(routes, path, depth) {
    routes = typeof routes !== "undefined" ? routes : null;
    path = typeof path !== "undefined" ? path : null;
    depth = typeof depth !== "undefined" ? depth : null;
    return (
      <>
        {routes.map((el) => {
          let badperms = false;
          if (el.private && !this.props.auth().isAuthenticated) {
            badperms = true;
          }
          if (el.perms) {
            el.perms.map((perm) => {
              if (this.props.auth().perms.indexOf(perm) === -1) {
                badperms = true;
              }
            });
          }
          if (el.revperms) {
            el.revperms.map((perm) => {
              if (this.props.auth().perms.indexOf(perm) !== -1) {
                badperms = true;
              }
            });
          }
          if (el.restrict) {
            if (el.restrict == "loggedin") {
              if (!this.props.auth().isAuthenticated) {
                return null;
              }
            } else if (el.restrict == "notloggedin") {
              if (this.props.auth().isAuthenticated) {
                return null;
              }
            }
          }
          if (el.hidden) {
            if (el.hidden === "loggedin") {
              if (this.props.auth().isAuthenticated) {
                return null;
              }
            } else if (el.hidden === "always") {
              return null;
            }
          }
          if (!badperms || this.props.auth().perms.includes("*")) {
            let lel;
            if (String(this.props.location.pathname).startsWith(el.url)) {
              lel = { backgroundColor: "#2980b9" };
            } else {
              lel = {};
            }

            return (
              <>
                <div
                  style={{
                    ...lel,
                    width: "100%",
                    paddingLeft: "15px",
                    paddingRight: "15px",
                    paddingTop: "15px",
                    paddingBottom: "15px",
                    marginLeft: "15px",
                    marginRight: "15px",
                    marginTop: "15px",
                    marginBottom: "15px",
                    margin: "0",
                  }}
                >
                  <Link
                    to={path + el.url}
                    key={"Link:" + path + el.url + el.name + depth}
                    onClick={el.onClick ? el.onClick : null}
                  >
                    <div
                      key={"Div: " + path + el.url + el.name + depth}
                      style={{
                        display: "list-item",
                        alignContent: "center",
                        textAlign: "center",
                        textDecoration: "none",
                        listStyle: "none",
                      }}
                    >
                      {settings.nav.showIcons ? el.icon : null}
                      {settings.nav.showName ? (
                        <p
                          style={{ color: "white", padding: "0", margin: "0" }}
                        >
                          {el.name}
                        </p>
                      ) : el.icon ? null : ( // fallback to text if has no icon and set to only show icons
                        <p>{el.name}</p>
                      )}
                    </div>
                  </Link>
                  {el.subs && el.subs.length > 0 ? (
                    this.navBuilder(el.subs, path + el.url, Number(depth) + 1)
                  ) : (
                    <></>
                  )}
                </div>
              </>
            );
          }
          if (el.subs && el.subs.length > 0) {
            return (
              <div key={"Sub Div: " + path + el.url + el.name + depth}>
                {this.navBuilder(el.subs, path + el.url, Number(depth) + 1)}
              </div>
            );
          }
        })}
      </>
    );
  }
  render() {
    return (
      <>
        <Media>
          <img
            width="150px"
            src="https://cdn.discordapp.com/attachments/763434469104549929/816884127596544011/Untitled-1.png"
          />
        </Media>
        {this.navBuilder(this.props.routes, "")}
      </>
    );
  }
}

const NavBarRoute = withRouter(NavBar);

const CustomInput = ({ label, bag, ...props }) => {
  // useField() returns [formik.getFieldProps(), formik.getFieldMeta()]
  // which we can spread on <input> and alse replace ErrorMessage entirely.
  const [field, meta] = useField(props);
  if (props.type === "datetime") {
    return (
      <>
        <CustomInput label={label} bag={bag} {...props} type="time" />
        <CustomInput label="" bag={bag} {...props} type="date" />
      </>
    );
  } else if (props.type === "date") {
    let date = new Date(bag.values[props.name]);
    return (
      <>
        <Form.Label
          key="label"
          htmlFor={props.id || props.name}
          style={{ fontSize: 30 }}
        >
          {label}
        </Form.Label>
        {meta.touched && meta.error ? (
          <Alert key="alert" variant={"danger"}>
            {meta.error}
          </Alert>
        ) : null}
        <Form.Control
          {...field}
          {...props}
          key="control"
          className="text-input"
          onChange={({ ...all }) => {
            try {
              let newDate = new Date(all.target.value);
              date.setUTCFullYear(newDate.getUTCFullYear());
              date.setUTCMonth(newDate.getUTCMonth());
              date.setUTCDate(newDate.getUTCDate());
              bag.setValues({
                ...bag.values,
                [props.name]: date.toISOString(),
              });
            } catch (err) {
              console.log(err);
              return;
            }
          }}
          value={`${("0000" + date.getFullYear()).slice(-4)}-${(
            "0" +
            (date.getMonth() + 1)
          ).slice(-2)}-${("0" + date.getDate()).slice(-2)}`}
        />
      </>
    );
  } else if (props.type === "time") {
    let date = new Date(bag.values[props.name]);
    return (
      <>
        <Form.Label
          key="label"
          htmlFor={props.id || props.name}
          style={{ fontSize: 30 }}
        >
          {label}
        </Form.Label>
        {meta.touched && meta.error ? (
          <Alert key="alert" variant={"danger"}>
            {meta.error}
          </Alert>
        ) : null}
        <Form.Control
          {...field}
          {...props}
          key="control"
          className="text-input"
          onChange={({ ...all }) => {
            let rg = all.target.value.match(/^(\d{2})\:(\d{2})\:?(\d{2})?$/);
            date.setHours(
              Number(rg[1]),
              Number(rg[2]),
              rg[3] ? Number(rg[3]) : null
            );
            bag.setValues({
              ...bag.values,
              [props.name]: date.toISOString(),
            });
          }}
          value={`${("0" + date.getHours()).slice(-2)}:${(
            "0" + date.getMinutes()
          ).slice(-2)}:${("0" + date.getSeconds()).slice(-2)}`}
        />
      </>
    );
  } else if (props.type == "checkbox") {
    return (
      <>
        <Form.Label
          key="label"
          htmlFor={props.id || props.name}
          style={{ fontSize: 30 }}
        >
          {label}
        </Form.Label>
        {meta.touched && meta.error ? (
          <Alert key="alert" variant={"danger"}>
            {meta.error}
          </Alert>
        ) : null}
        <Form.Control
          {...field}
          {...props}
          key="control"
          className="text-input"
          onChange={({ ...all }) => {
            bag.setValues({
              ...bag.values,
              [props.name]: !bag.values[props.name],
            });
          }}
        />
      </>
    );
  }
  return (
    <>
      <Form.Label
        key="label"
        htmlFor={props.id || props.name}
        style={{ fontSize: 30 }}
      >
        {label}
      </Form.Label>
      {meta.touched && meta.error ? (
        <Alert key="alert" variant={"danger"}>
          {meta.error}
        </Alert>
      ) : null}
      <Form.Control
        {...field}
        {...props}
        onChange={({ target, ...all }) => {
          bag.setValues({ ...bag.values, [props.name]: target.value });
        }}
        key="control"
        className="text-input"
      />
    </>
  );
};

class Main extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <>
        <Row>
          {this.props.auth().isAuthenticated ? (
            <div
              className="position-sticky sticky-top"
              style={{
                width: "150px",
                position: "absolute",
                height: "100vh",
                bottom: "0",
                backgroundColor: "#3498db",
                padding: "0",
              }}
              xs={1}
            >
              <NavBarRoute
                {...this.props}
                auth={this.props.auth}
                routes={this.props.routes}
              />
            </div>
          ) : null}

          <Col style={{ margin: "30px" }} className="p-3">
            <Routes auth={this.props.auth} routes={this.props.routes} />
          </Col>
        </Row>
      </>
    );
  }
}

class App extends React.Component {
  setAuthed(to) {
    this.setState({
      auth: { ...this.state.auth, isAuthenticated: to },
    });
  }
  getAuth() {
    return this.state.auth;
  }
  setLoading(to) {
    this.setState({ ...this.state, loading: to });
  }
  constructor(props) {
    super(props);
    const that = this;
    this.state = {
      loading: true,
      auth: {
        isAuthenticated: false,
        token: "",
        perms: [],
        user: {},
        loginCbs: [],
        callLoginCbs() {
          this.loginCbs.map((fun) => fun());
        },
        login({ email, password }) {
          console.log(`auth.login(emai: ${email}, password: ${password})`);
          return new Promise((resolve, reject) => {
            this.makeReq(false, "http://ntukfc.me/api/auth/login", "POST", {
              email: email,
              password: password,
            })
              .then((data) => {
                if (data.access_token) {
                  this.checkToken(data.access_token)
                    .then((user) => {
                      this.user = user;
                      return user;
                    })
                    .then((user) => {
                      this.checkPerms()
                        .then(() => {
                          that.setAuthed(true);
                          resolve(user);
                        })
                        .catch((error) => {
                          console.log(
                            `ERROR: auth.login(${email}, ${password}): ${error}`
                          );
                          reject(error);
                        });
                    })
                    .catch((error) => {
                      console.log(
                        `ERROR: auth.login(${email}, ${password}): ${error}`
                      );
                      reject(error);
                    });
                } else {
                  console.log(
                    `ERROR: auth.login(${email}, ${password}): ${data}`
                  );
                  reject(data);
                }
              })
              .catch((error) => {
                console.log(
                  `ERROR: auth.login(${email}, ${password}): ${error}`
                );
                reject(error);
              });
          });
        },
        checkPerms() {
          console.log(`auth.checkPerms()`);
          return new Promise((resolve, reject) => {
            this.makeReq(true, "http://ntukfc.me/api/groups/", "GET")
              .then((groupsInfo) => {
                groupsInfo = groupsInfo.groups;
                this.user.groups.forEach((u) => {
                  groupsInfo.forEach((g) => {
                    if (u == g.group_name) {
                      this.perms = this.perms.concat(g.group_perms);
                    }
                  });
                });
              })
              .then(() => {
                console.log(this.perms);
                resolve();
              })
              .catch((error) => {
                console.log(`ERROR: auth.checkPerms(): ${error}`);
                reject(error);
              });
          });
        },
        restoreSession() {
          console.log(`auth.restoreSession()`);
          return new Promise((resolve, reject) => {
            let localToken = localStorage.getItem("token");
            if (localToken) {
              this.checkToken(localToken)
                .then((user) => {
                  this.user = user;
                  return user;
                })
                .then(() => {
                  this.checkPerms().then(() => {
                    that.setAuthed(true);
                    resolve();
                  });
                })
                .catch((error) => {
                  console.log(`ERROR: restoreSession(): ${error}`);
                  reject(error);
                });
            } else {
              reject("No token exists");
            }
          });
        },
        checkToken(token) {
          return new Promise((resolve, reject) => {
            console.log(`auth.checkToken(${token})`);
            if (!token) {
              reject("No token");
            }
            this.token = token;
            this.isAuthenticated = true;
            this.makeReq(true, "http://ntukfc.me/api/account", "GET")
              .then((result) => {
                if (result.status == 200) {
                  localStorage.setItem("token", this.token);
                  resolve(result.user);
                } else {
                  localStorage.setItem("token", "");
                  this.token = "";
                  this.isAuthenticated = false;
                  reject("result code not 200");
                }
              })
              .catch((error) => {
                this.token = "";
                this.isAuthenticated = false;
                console.log(`ERROR: checkToken(${token}): ${error}`);
                reject(error);
              });
          });
        },
        makeReq(authed, url, method, data) {
          return new Promise((resolve, reject) => {
            console.log(`auth.makeReq(${authed}, ${url}, ${method}, ${data})`);
            console.log(data);
            if (authed && !this.isAuthenticated) {
              reject("Not authenticated");
            }

            var myHeaders = new Headers();
            if (authed) {
              myHeaders.append("auth-token", this.token);
            }
            myHeaders.append("Content-Type", "application/json");
            let raw = JSON.stringify(data);
            var requestOptions = {
              method: method,
              headers: myHeaders,
              redirect: "follow",
            };
            if (data) {
              requestOptions.body = raw;
            }
            fetch(url, requestOptions)
              .then((response) => response.text())
              .then((result) => {
                resolve(JSON.parse(result));
              })
              .catch((error) => {
                console.log(
                  `ERROR: auth.makeReq(${authed}, ${url}, ${method}, ${data}): ${error}`
                );
                reject(error);
              });
          });
        },
        logout() {
          this.user = "";
          this.token = "";
          localStorage.setItem("token", "");
          this.isAuthenticated = false;
          window.location.reload();
        },
      },
    };
    this.routes = [
      {
        name: "My Prescriptions",
        url: "/mypersc",
        hideInSubs: true,
        restrict: "loggedin",
        revperms: ["perscription.edit"],
        comp: (
          <CustomList
            auth={() => this.getAuth()}
            render={(data, noData) => {
              if (data.status !== 200 || data.perscriptions.length < 1) {
                return noData();
              } else {
                data = data.perscriptions;
                return (
                  <Table>
                    <thead>
                      <tr>
                        <th>Medication</th>
                        <th>Collected</th>
                        <th>Collection Date</th>
                        <th>View</th>
                      </tr>
                    </thead>
                    <tbody>
                      {data.map((persc) => {
                        if (!persc || persc.medication_data.length < 1) return;
                        let colDate = new Date(persc.collection_time);
                        return (
                          <tr key={persc._id}>
                            <td>
                              {capitalize(persc.medication_data[0].med_name)}
                            </td>
                            <td>{persc.collected ? "Yes" : "No"}</td>
                            <td>
                              {colDate.getFullYear() === 1970
                                ? "Not assigned"
                                : `${colDate.getDate()}/${
                                    colDate.getMonth() + 1
                                  }/${colDate.getFullYear()} @ ${(
                                    "0" + colDate.getHours()
                                  ).slice(-2)}:${(
                                    "0" + colDate.getMinutes()
                                  ).slice(-2)}`}
                            </td>
                            <td>
                              <Link to={"/mypersc/view/" + persc._id}>
                                View
                              </Link>
                            </td>
                          </tr>
                        );
                      })}
                    </tbody>
                  </Table>
                );
              }
            }}
            title="My Prescriptions"
            url={`http://ntukfc.me/api/perscription/current_user`}
          />
        ),
        subs: [
          {
            name: "View",
            url: "/view/:id",
            restrict: "loggedin",
            hidden: "loggedin",
            hideInSubs: true,
            comp: (
              <CustomView
                auth={() => this.getAuth()}
                title="Prescription"
                render={(data) => {
                  data = data.perscription;
                  let colDate = new Date(data.collection_time);
                  let creDate = new Date(data.created_at);
                  return (
                    <>
                      <b>Prescription ID: </b> <p>{data._id}</p>
                      <b>Patient Email: </b> <p>{data.patient_email}</p>
                      <b>Created At: </b>
                      <p>{`${("0" + creDate.getDate()).slice(-2)}/${(
                        "0" +
                        creDate.getMonth() +
                        1
                      ).slice(-2)}/${creDate.getFullYear()} @ ${(
                        "0" + creDate.getHours()
                      ).slice(-2)}:${("0" + creDate.getMinutes()).slice(
                        -2
                      )}`}</p>
                      <b>Collection Time: </b>{" "}
                      <p>
                        {colDate.getFullYear() == 1970
                          ? "Not assigned"
                          : `${("0" + colDate.getDate()).slice(-2)}/${(
                              "0" +
                              colDate.getMonth() +
                              1
                            ).slice(-2)}/${colDate.getFullYear()} @ ${(
                              "0" + colDate.getHours()
                            ).slice(-2)}:${("0" + colDate.getMinutes()).slice(
                              -2
                            )}`}
                      </p>
                      <b>Bloodtest Needed: </b>{" "}
                      <p>{data.bloodtest_needed ? "Yes" : "No"}</p>
                      <b>Bloodtest Completed: </b>{" "}
                      <p>{data.bloodtest_completed ? "Yes" : "No"}</p>
                      <b>Medication Name: </b>{" "}
                      <p>{capitalize(data.medication_data[0].med_name)}</p>
                      <b>Days Without Blood Test: </b>{" "}
                      <p>
                        {data.medication_data[0].med_blood_work_days_without
                          ? data.medication_data[0].med_blood_work_days_without
                          : "No Data"}
                      </p>
                    </>
                  );
                }}
                url="http://ntukfc.me/api/perscription"
              />
            ),
          },
        ],
      },
      {
        name: "All Prescriptions",
        url: "/list",
        perms: ["view.perscription.*"],
        restrict: "loggedin",
        comp: (
          <>
            <CustomList
              auth={() => this.getAuth()}
              modData={(data) => {
                let newObj = {
                  ...data,
                  perscriptions: data.perscriptions.map((el) => {
                    if (
                      el &&
                      el.collection_time &&
                      !String(el.collection_time).startsWith("1970")
                    )
                      return el;
                  }),
                };
                newObj.perscriptions.sort((a, b) => {
                  let datea = new Date(a.collection_time);
                  let dateb = new Date(b.collection_time);
                  return datea - dateb;
                });
                return newObj;
              }}
              render={(data, noData, setStateCl, stateCl) => {
                if (data.status !== 200 || data.perscriptions.length < 1) {
                  return noData();
                } else {
                  data = data.perscriptions;
                  if (stateCl.ordering) {
                    if (stateCl.ordering.by === "patient") {
                      data.sort((a, b) => {
                        if (stateCl.ordering.asc) {
                          return a.patient_email < b.patient_email
                            ? 1
                            : a.patient_email == b.patient_email
                            ? 0
                            : -1;
                        }
                        return b.patient_email < a.patient_email
                          ? 1
                          : b.patient_email == a.patient_email
                          ? 0
                          : -1;
                      });
                    }
                    if (stateCl.ordering.by === "coldate") {
                      data.sort((a, b) => {
                        let datea = new Date(a.collection_time);
                        let dateb = new Date(b.collection_time);
                        if (stateCl.ordering.asc) {
                          return datea - dateb;
                        }
                        return dateb - datea;
                      });
                    }
                    if (stateCl.ordering.by === "blood") {
                      data.sort((a, b) => {
                        if (stateCl.ordering.asc) {
                          return b.bloodtest_needed - a.bloodtest_needed;
                        }
                        return a.bloodtest_needed - b.bloodtest_needed;
                      });
                    }
                    if (stateCl.ordering.by === "col") {
                      data.sort((a, b) => {
                        if (stateCl.ordering.asc) {
                          return b.collected - a.collected;
                        }
                        return a.collected - b.collected;
                      });
                    }
                  }
                  return (
                    <>
                      <>
                        <Row>
                          <Col md={{ span: 2 }}>
                            <Form.Group controlId="exampleForm.ControlInput1">
                              <Form.Control
                                type="text"
                                placeholder="filter..."
                                onChange={({ ...all }) => {
                                  setStateCl({
                                    ...stateCl,
                                    filter: all.target.value,
                                  });
                                }}
                              />
                            </Form.Group>
                          </Col>
                          <Col md={{ span: 2 }}>
                            <Form.Check
                              label="hide collected"
                              checked={stateCl.hideCollected}
                              onClick={() => {
                                setStateCl({
                                  ...stateCl,
                                  hideCollected: !stateCl.hideCollected,
                                });
                              }}
                            />
                          </Col>
                        </Row>
                      </>
                      <Table>
                        <thead>
                          <tr>
                            <th
                              onClick={() =>
                                setStateCl({
                                  ...stateCl,
                                  ordering: {
                                    by: "patient",
                                    asc: stateCl.ordering
                                      ? stateCl.ordering.asc
                                        ? !stateCl.ordering.asc
                                        : true
                                      : true,
                                  },
                                })
                              }
                            >
                              Patient Email
                            </th>
                            <th
                              onClick={() => {
                                setStateCl({
                                  ...stateCl,
                                  ordering: {
                                    by: "blood",
                                    asc: stateCl.ordering
                                      ? stateCl.ordering.asc
                                        ? !stateCl.ordering.asc
                                        : true
                                      : true,
                                  },
                                });
                              }}
                            >
                              Bloodwork
                            </th>
                            <th
                              onClick={() => {
                                setStateCl({
                                  ...stateCl,
                                  ordering: {
                                    by: "coldate",
                                    asc: stateCl.ordering
                                      ? stateCl.ordering.asc
                                        ? !stateCl.ordering.asc
                                        : true
                                      : true,
                                  },
                                });
                              }}
                            >
                              Collection Time
                            </th>
                            <th>Medication</th>
                            <th
                              onClick={() => {
                                setStateCl({
                                  ...stateCl,
                                  ordering: {
                                    by: "col",
                                    asc: stateCl.ordering
                                      ? stateCl.ordering.asc
                                        ? !stateCl.ordering.asc
                                        : true
                                      : true,
                                  },
                                });
                              }}
                            >
                              Collect
                            </th>
                            <th>View</th>
                          </tr>
                        </thead>
                        <tbody>
                          {data.map((persc, i) => {
                            if (!persc || persc.medication_data.length < 1)
                              return;
                            let colDate = new Date(persc.collection_time);
                            let bloodStr;
                            if (persc.medication_data[0].med_blood_work == 1) {
                              bloodStr = "Not yet required";
                            } else if (
                              persc.bloodtest_needed &&
                              persc.medication_data[0].med_blood_work == 2
                            ) {
                              bloodStr = "Required";
                            } else {
                              bloodStr = "Not required";
                            }
                            if (stateCl.hideCollected && persc.collected)
                              return null;
                            if (stateCl.filter) {
                              if (
                                !String(persc.patient_email)
                                  .toLowerCase()
                                  .startsWith(stateCl.filter)
                              ) {
                                return null;
                              }
                            }
                            return (
                              <tr key={persc._id}>
                                <td>{persc.patient_email}</td>
                                <td>{bloodStr}</td>
                                <td>
                                  {colDate.getFullYear() === 1970
                                    ? "Not assigned"
                                    : `${colDate.getDate()}/${
                                        colDate.getMonth() + 1
                                      }/${colDate.getFullYear()} @ ${(
                                        "0" + colDate.getHours()
                                      ).slice(-2)}:${(
                                        "0" + colDate.getMinutes()
                                      ).slice(-2)}`}
                                </td>
                                <td>
                                  {persc.medication_data.map((med) => {
                                    return capitalize(med.med_name) + " ";
                                  })}
                                </td>
                                <td
                                  onClick={() => {
                                    if (!persc.collected) {
                                      setStateCl({
                                        ...(stateCl.data.perscriptions[
                                          i
                                        ].status = "..."),
                                      });
                                      this.state.auth
                                        .makeReq(
                                          true,
                                          `http://ntukfc.me/api/perscription/edit/${persc._id}`,
                                          "PATCH",
                                          { ...persc, collected: true }
                                        )
                                        .then((data) => {
                                          setStateCl({
                                            ...(stateCl.data.perscriptions[
                                              i
                                            ].status = "Done!"),
                                          });
                                          setStateCl({
                                            ...(stateCl.data.perscriptions[
                                              i
                                            ].collected = true),
                                          });
                                        })
                                        .catch((error) => {
                                          setStateCl({
                                            ...(stateCl.data.perscriptions[
                                              i
                                            ].status = "Error!"),
                                          });
                                        });
                                    }
                                  }}
                                >
                                  {persc.status ? (
                                    persc.status
                                  ) : persc.collected ? (
                                    "Already Collected"
                                  ) : persc.bloodtest_needed ? (
                                    "Bloodwork required"
                                  ) : (
                                    <Button>Collect</Button>
                                  )}
                                </td>
                                <td>
                                  <Link to={"/view/" + persc._id}>View</Link>
                                </td>
                              </tr>
                            );
                          })}
                        </tbody>
                      </Table>
                    </>
                  );
                }
              }}
              title="Upcoming Prescriptions"
              url="http://ntukfc.me/api/perscription"
            />
            <CustomList
              auth={() => this.getAuth()}
              modData={(data) => {
                let newObj = {
                  ...data,
                  perscriptions: data.perscriptions.map((el) => {
                    if (
                      el &&
                      el.bloodtest_needed &&
                      (!el.collection_time ||
                        String(el.collection_time).startsWith("1970"))
                    )
                      return el;
                  }),
                };
                return newObj;
              }}
              render={(data, noData, setStateCl, stateCl) => {
                if (data.status !== 200 || data.perscriptions.length < 1) {
                  return noData();
                } else {
                  data = data.perscriptions;
                  if (stateCl.ordering) {
                    if (stateCl.ordering.by === "patient") {
                      data.sort((a, b) => {
                        if (stateCl.ordering.asc) {
                          return a.patient_email < b.patient_email
                            ? 1
                            : a.patient_email == b.patient_email
                            ? 0
                            : -1;
                        }
                        return b.patient_email < a.patient_email
                          ? 1
                          : b.patient_email == a.patient_email
                          ? 0
                          : -1;
                      });
                    }
                    if (stateCl.ordering.by === "coldate") {
                      data.sort((a, b) => {
                        let datea = new Date(a.collection_time);
                        let dateb = new Date(b.collection_time);
                        if (stateCl.ordering.asc) {
                          return datea - dateb;
                        }
                        return dateb - datea;
                      });
                    }
                    if (stateCl.ordering.by === "blood") {
                      data.sort((a, b) => {
                        if (stateCl.ordering.asc) {
                          return b.bloodtest_needed - a.bloodtest_needed;
                        }
                        return a.bloodtest_needed - b.bloodtest_needed;
                      });
                    }
                    if (stateCl.ordering.by === "col") {
                      data.sort((a, b) => {
                        if (stateCl.ordering.asc) {
                          return b.collected - a.collected;
                        }
                        return a.collected - b.collected;
                      });
                    }
                  }
                  return (
                    <>
                      <>
                        <Row>
                          <Col md={{ span: 2 }}>
                            <Form.Group controlId="exampleForm.ControlInput1">
                              <Form.Control
                                type="text"
                                placeholder="filter..."
                                onChange={({ ...all }) => {
                                  setStateCl({
                                    ...stateCl,
                                    filter: all.target.value,
                                  });
                                }}
                              />
                            </Form.Group>
                          </Col>
                          <Col md={{ span: 2 }}>
                            <Form.Check
                              label="hide collected"
                              checked={stateCl.hideCollected}
                              onClick={() => {
                                setStateCl({
                                  ...stateCl,
                                  hideCollected: !stateCl.hideCollected,
                                });
                              }}
                            />
                          </Col>
                        </Row>
                      </>
                      <Table>
                        <thead>
                          <tr>
                            <th
                              onClick={() =>
                                setStateCl({
                                  ...stateCl,
                                  ordering: {
                                    by: "patient",
                                    asc: stateCl.ordering
                                      ? stateCl.ordering.asc
                                        ? !stateCl.ordering.asc
                                        : true
                                      : true,
                                  },
                                })
                              }
                            >
                              Patient Email
                            </th>
                            <th
                              onClick={() => {
                                setStateCl({
                                  ...stateCl,
                                  ordering: {
                                    by: "blood",
                                    asc: stateCl.ordering
                                      ? stateCl.ordering.asc
                                        ? !stateCl.ordering.asc
                                        : true
                                      : true,
                                  },
                                });
                              }}
                            >
                              Bloodwork
                            </th>
                            <th>Medication</th>
                            <th>Send Reminder</th>
                            <th>View</th>
                          </tr>
                        </thead>
                        <tbody>
                          {data.map((persc, i) => {
                            if (!persc || persc.medication_data.length < 1)
                              return;
                            let colDate = new Date(persc.collection_time);
                            let bloodStr;
                            if (
                              persc.bloodtest_needed &&
                              persc.medication_data[0].med_blood_work == 1
                            ) {
                              bloodStr = "Not yet required";
                            } else if (
                              persc.bloodtest_needed &&
                              persc.medication_data[0].med_blood_work == 2
                            ) {
                              bloodStr = "Required";
                            } else {
                              bloodStr = "Not required";
                            }
                            if (stateCl.hideCollected && persc.collected)
                              return null;
                            if (stateCl.filter) {
                              if (
                                !String(persc.patient_email)
                                  .toLowerCase()
                                  .startsWith(stateCl.filter)
                              ) {
                                return null;
                              }
                            }

                            return (
                              <tr key={persc._id}>
                                <td>{persc.patient_email}</td>
                                <td>{bloodStr}</td>
                                <td>
                                  {persc.medication_data.map((med) => {
                                    return capitalize(med.med_name) + " ";
                                  })}
                                </td>

                                <td
                                  onClick={() => {
                                    if (!persc.collected) {
                                      setStateCl({
                                        ...(stateCl.data.perscriptions[
                                          i
                                        ].remstatus = "..."),
                                      });
                                      this.state.auth
                                        .makeReq(
                                          true,
                                          `http://ntukfc.me/api/sms/send/${persc.patient_email}`,
                                          "POST",
                                          {
                                            message:
                                              "This is a reminder that you need a blood test before you collect",
                                          }
                                        )
                                        .then((data) => {
                                          setStateCl({
                                            ...(stateCl.data.perscriptions[
                                              i
                                            ].remstatus = "Sent!"),
                                          });
                                        })
                                        .catch((error) => {
                                          setStateCl({
                                            ...(stateCl.data.perscriptions[
                                              i
                                            ].remstatus = "Error!"),
                                          });
                                        });
                                    }
                                  }}
                                >
                                  {persc.remstatus ? (
                                    persc.remstatus
                                  ) : (
                                    <Button>Send</Button>
                                  )}
                                </td>
                                <td>
                                  <Link to={"/view/" + persc._id}>View</Link>
                                </td>
                              </tr>
                            );
                          })}
                        </tbody>
                      </Table>
                    </>
                  );
                }
              }}
              title="Required Bloodwork"
              url="http://ntukfc.me/api/perscription"
            />
          </>
        ),
      },
      {
        name: "Create Prescriptions",
        url: "/new",
        restrict: "loggedin",
        perms: ["perscription.create"],
        comp: (
          <>
            <CustomForm
              centerTitle={true}
              auth={this.state.auth}
              title="Create Prescription"
              url="http://ntukfc.me/api/perscription/new"
              submit={(values, props, { setSubmitting, setStatus, ...all }) => {
                let tosend = {
                  patient_email: values.email,
                  medication_id: values.med._id,
                };
                props.auth
                  .makeReq(
                    true,
                    "http://ntukfc.me/api/perscription/new",
                    "POST",
                    tosend
                  )
                  .then((data) => {
                    setStatus(data);
                    setSubmitting(false);
                  })
                  .catch((error) => {
                    setStatus(error);
                  });
              }}
              render={(bag, props, setStateCf) => {
                return (
                  <>
                    <Col md={{ offset: 4, span: 4 }}>
                      <CustomInput
                        label="Email"
                        bag={bag}
                        name="email"
                        onChange={({ target, ...all }) => {
                          bag.setValues({
                            ...bag.values,
                            name: target.value,
                          });
                        }}
                        type="text"
                        placeholder="user@email.com"
                        disabled={false}
                      />
                      <CustomDropdown
                        auth={this.state.auth}
                        name="med"
                        label="Medication"
                        onSelect={(e, state, setState) => {
                          e = JSON.parse(e);
                          let temp = state.data.map((el) => {
                            if (el._id === e._id) {
                              setState({ title: capitalize(el.med_name) });
                              bag.setValues({ ...bag.values, med: el });
                              return { ...el, active: true };
                            } else {
                              return { ...el, active: false };
                            }
                          });
                          setState({ data: temp });
                        }}
                        getData={(setStateCdd, auth) => {
                          return new Promise((resolve, reject) => {
                            this.state.auth
                              .makeReq(
                                true,
                                "http://ntukfc.me/api/medication/",
                                "GET"
                              )
                              .then((data) => {
                                resolve(data.medications);
                              })
                              .catch((error) => {
                                reject(error);
                              });
                          });
                        }}
                      />
                      <div style={{ marginTop: "30px" }}>
                        <Button
                          style={{ marginRight: "10px" }}
                          type="submit"
                          disabled={bag.isSubmitting}
                        >
                          Submit
                        </Button>
                        <Button type="button" onClick={bag.handleReset}>
                          Reset
                        </Button>
                      </div>
                    </Col>
                  </>
                );
              }}
              schema={Yup.object({
                email: Yup.string()
                  .email("Invalid email addresss")
                  .required("Required"),
                med: Yup.object().required("Required"),
              })}
              inputs={[
                {
                  label: "Name",
                  name: "email",
                  text: "text",
                  disabled: true,
                },
                {
                  label: "Med",
                  name: "med",
                  disabled: true,
                  type: "array",
                },
              ]}
            />
          </>
        ),
      },
      {
        name: "View",
        url: "/view/:id",
        restrict: "loggedin",
        hidden: "loggedin",
        hideInSubs: true,
        perms: ["view.perscription.*"],
        subs: [
          {
            name: "Edit",
            url: "/edit",
            hideInSubs: true,
            hidden: "loggedin",
            restrict: "loggedin",
            comp: (
              <CustomForm
                title="Edit Prescription"
                render={(bag, props) => {
                  return (
                    <>
                      {props.inputs.map((input) => {
                        if (input.type) {
                          if (input.type == "array") {
                            return (
                              <>
                                <CustomDropdown
                                  auth={this.state.auth}
                                  name="med"
                                  label="Medication"
                                  onMount={(setMed) => {
                                    setMed(
                                      capitalize(
                                        bag.values[input.name][0].med_name
                                      )
                                    );
                                  }}
                                  onSelect={(e, state, setState) => {
                                    e = JSON.parse(e);
                                    let temp = state.data.map((el) => {
                                      if (el._id === e._id) {
                                        setState({
                                          title: capitalize(el.med_name),
                                        });
                                        bag.setValues({
                                          ...bag.values,
                                          med: el,
                                        });
                                        return { ...el, active: true };
                                      } else {
                                        return { ...el, active: false };
                                      }
                                    });
                                    setState({ data: temp });
                                  }}
                                  getData={(setStateCdd, auth) => {
                                    return new Promise((resolve, reject) => {
                                      this.state.auth
                                        .makeReq(
                                          true,
                                          "http://ntukfc.me/api/medication/",
                                          "GET"
                                        )
                                        .then((data) => {
                                          resolve(data.medications);
                                        })
                                        .catch((error) => {
                                          reject(error);
                                        });
                                    });
                                  }}
                                />
                              </>
                            );
                          }
                        }
                        return (
                          <CustomInput
                            {...input}
                            bag={bag}
                            key={JSON.stringify(input)}
                          />
                        );
                      })}{" "}
                      <div style={{ marginTop: "30px" }}>
                        <Button
                          style={{ marginRight: "10px" }}
                          type="submit"
                          disabled={bag.isSubmitting}
                        >
                          Submit
                        </Button>
                        <Button type="button" onClick={bag.handleReset}>
                          Reset
                        </Button>
                      </div>
                    </>
                  );
                }}
                auth={() => this.getAuth()}
                inputs={[
                  {
                    label: "ID",
                    name: "_id",
                    text: "text",
                    placeholder: "ID",
                    disabled: true,
                  },
                  {
                    label: "Email",
                    name: "patient_email",
                    text: "email",
                    placeholder: "email",
                    disabled: true,
                  },
                  {
                    label: "Created at",
                    name: "created_at",
                    text: "text",
                    type: "datetime",
                    disabled: true,
                  },
                  {
                    label: "Collection Time",
                    name: "collection_time",
                    type: "datetime",
                    text: "text",
                  },
                  {
                    label: "Bloodtest Needed",
                    name: "bloodtest_needed",
                    type: "checkbox",
                    disabled: true,
                  },
                  {
                    label: "Bloodtest Completed",
                    name: "bloodtest_completed",
                    type: "checkbox",
                  },
                  {
                    label: "Medication Name",
                    name: "medication_data",
                    type: "array",
                  },
                ]}
                url="http://ntukfc.me/api/perscription/edit/:id"
                submit={(
                  values,
                  props,
                  { setSubmitting, setStatus, ...rest }
                ) => {
                  this.state.auth
                    .makeReq(
                      true,
                      `http://ntukfc.me/api/perscription/edit/${props.match.params.id}`,
                      "PATCH",
                      { ...values, medication_id: values.med._id }
                    )
                    .then((data) => {
                      setSubmitting(false);
                      setStatus(data);
                    })
                    .catch((error) => {
                      setSubmitting(false);
                      setStatus(JSON.stringify(error));
                    });
                }}
              />
            ),
          },
        ],
        comp: (
          <CustomView
            auth={() => this.getAuth()}
            title="Prescription"
            render={(data) => {
              data = data.perscription;
              let colDate = new Date(data.collection_time);
              let creDate = new Date(data.created_at);
              return (
                <>
                  <b>Prescription ID: </b> <p>{data._id}</p>
                  <b>Patient Email: </b> <p>{data.patient_email}</p>{" "}
                  <b>Created At: </b>
                  <p>{`${("0" + creDate.getDate()).slice(-2)}/${(
                    "0" +
                    creDate.getMonth() +
                    1
                  ).slice(-2)}/${creDate.getFullYear()} @ ${(
                    "0" + creDate.getHours()
                  ).slice(-2)}:${("0" + creDate.getMinutes()).slice(-2)}`}</p>
                  <b>Collection Time: </b>{" "}
                  <p>
                    {colDate.getFullYear() == 1970
                      ? "Not assigned"
                      : `${("0" + colDate.getDate()).slice(-2)}/${(
                          "0" +
                          colDate.getMonth() +
                          1
                        ).slice(-2)}/${colDate.getFullYear()} @ ${(
                          "0" + colDate.getHours()
                        ).slice(-2)}:${("0" + colDate.getMinutes()).slice(-2)}`}
                  </p>
                  <b>Bloodtest Needed: </b>{" "}
                  <p>{data.bloodtest_needed ? "Yes" : "No"}</p>
                  <b>Bloodtest Completed: </b>{" "}
                  <p>{data.bloodtest_completed ? "Yes" : "No"}</p>
                  <b>Medication Name: </b>{" "}
                  <p>{capitalize(data.medication_data[0].med_name)}</p>
                  <b>Days Without Blood Test: </b>{" "}
                  <p>
                    {data.medication_data[0].med_blood_work_days_without
                      ? data.medication_data[0].med_blood_work_days_without
                      : "No Data"}
                  </p>
                  <Link
                    to={(loc) => ({
                      ...loc,
                      pathname: `${loc.pathname}/edit`,
                      state: {
                        prefil: data,
                      },
                    })}
                  >
                    Edit
                  </Link>
                </>
              );
            }}
            url="http://ntukfc.me/api/perscription"
          />
        ),
      },
      {
        name: "Manage Registrations",
        url: "/managerequests",
        perms: ["requests.grant", "requests.deny"],
        hideInSubs: true,
        restrict: "loggedin",
        // comp: <ListRequests auth={() => this.getAuth()} />,
        comp: (
          <CustomList
            title="Requests"
            url="http://ntukfc.me/api/account/staff/account_requests"
            auth={() => this.getAuth()}
            render={(data, noData, setStateCl, stateCl) => {
              if (data.status !== 200 || data.requests.length < 1) {
                return noData("No active registrations");
              } else {
                data = data.requests;
                return (
                  <Table>
                    <thead>
                      <tr>
                        <th>Email</th>
                        <th>NHS Number</th>
                        <th>Reason</th>
                        <th>Approve</th>
                        <th>Deny</th>
                      </tr>
                    </thead>
                    <tbody>
                      {data.map((el, i) => {
                        return (
                          <tr key={JSON.stringify(el)}>
                            <td>{el.email}</td>
                            <td>{el.nhs_number}</td>
                            <td>{el.reason}</td>
                            <td
                              onClick={() => {
                                if (!el.status) {
                                  setStateCl({
                                    ...(stateCl.data.requests[i].status =
                                      "..."),
                                  });
                                  this.state.auth
                                    .makeReq(
                                      true,
                                      `http://ntukfc.me/api/account/staff/grant_access/${el.email}`,
                                      "POST",
                                      {
                                        groups: ["patient"],
                                      }
                                    )
                                    .then((data) => {
                                      setStateCl({
                                        ...(stateCl.data.requests[i].status =
                                          "Approved"),
                                      });
                                    })
                                    .catch((error) => {
                                      setStateCl({
                                        ...(stateCl.data.requests[i].status =
                                          "Error"),
                                      });
                                    });
                                }
                              }}
                            >
                              {el.status ? (
                                el.status
                              ) : (
                                <Button variant="success">Approve</Button>
                              )}
                            </td>
                            <td
                              onClick={() => {
                                if (!el.status) {
                                  setStateCl({
                                    ...(stateCl.data.requests[i].status =
                                      "..."),
                                  });
                                  this.state.auth
                                    .makeReq(
                                      true,
                                      `http://ntukfc.me/api/account/staff/deny_access/${el.email}`,
                                      "POST",
                                      {
                                        message:
                                          "This is a reminder that you need a blood test before you collect",
                                      }
                                    )
                                    .then((data) => {
                                      setStateCl({
                                        ...(stateCl.data.requests[i].status =
                                          "Denied"),
                                      });
                                    })
                                    .catch((error) => {
                                      setStateCl({
                                        ...(stateCl.data.requests[i].status =
                                          "Error"),
                                      });
                                    });
                                }
                              }}
                            >
                              {el.status ? (
                                el.status
                              ) : (
                                <Button variant="danger">Deny</Button>
                              )}
                            </td>
                          </tr>
                        );
                      })}
                    </tbody>
                  </Table>
                );
              }
            }}
          />
        ),
        subs: [
          {
            name: "View Request",
            url: "/view/:reqId",
            perms: ["requests.grant", "requests.deny"],
            hidden: "always",
            restrict: "loggedin",
            comp: <ViewRequest auth={() => this.getAuth()} />,
          },
        ],
      },
      {
        name: "User Management",
        url: "/usermanagement",
        restrict: "loggedin",
        comp: (
          <CustomList
            modData={(data) => {
              return data;
            }}
            auth={() => this.getAuth()}
            render={(data, noData) => {
              if (data.status !== 200 || data.users.length < 1) {
                return noData();
              } else {
                data = data.users;
                return (
                  <Table>
                    <thead>
                      <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Phone</th>
                        <th>Edit</th>
                        <th>View</th>
                      </tr>
                    </thead>
                    <tbody>
                      {data.map((user) => {
                        return (
                          <tr key={user._id}>
                            <td>{capitalize(user.first_name)}</td>
                            <td>{capitalize(user.last_name)}</td>
                            <td>{user.phone_no}</td>
                            <td>
                              <Link
                                to={{
                                  pathname: `/usermanagement/view/${user.email}/edit`,
                                  state: {
                                    prefil: user,
                                  },
                                }}
                              >
                                Edit
                              </Link>
                            </td>
                            <td>
                              <Link to={"/usermanagement/view/" + user.email}>
                                View
                              </Link>
                            </td>
                          </tr>
                        );
                      })}
                    </tbody>
                  </Table>
                );
              }
            }}
            title="Users"
            url="http://ntukfc.me/api/account/staff/accounts"
          />
        ),
        hideInSubs: true,
        perms: ["view.patient.*"],
        subs: [
          {
            name: "View User",
            url: "/view/:id",
            hideInSubs: true,
            hidden: "always",
            restrict: "loggedin",
            comp: (
              <CustomView
                auth={() => this.getAuth()}
                title="View user"
                url="http://ntukfc.me/api/account/staff/accounts"
                render={(data) => {
                  data = data.user;
                  let lastBlood = new Date(data.last_blood_test);
                  return (
                    <>
                      <b>ID: </b> <p>{data._id}</p>
                      <b>First Name: </b> <p>{capitalize(data.first_name)}</p>
                      <b>Last Name: </b> <p>{capitalize(data.last_name)}</p>
                      <b>Email: </b> <p>{data.email}</p>
                      <b>Phone Number: </b> <p>{data.phone_no}</p>
                      <b>NHS Number: </b> <p>{data.nhs_number}</p>
                      <b>Last Blood Test: </b>{" "}
                      <p>
                        {lastBlood.getFullYear() == 1970
                          ? "Not assigned"
                          : data.last_blood_test}
                      </p>
                      <b>Groups: </b>{" "}
                      <p>
                        {data.groups.map((group) => {
                          return group;
                        })}
                      </p>
                      <Row>
                        <Link
                          style={{ paddingRight: "10px" }}
                          to={(loc) => ({
                            ...loc,
                            pathname: `/new`,
                            state: {
                              prefil: data,
                            },
                          })}
                        >
                          <Button>Add New Prescription</Button>
                        </Link>{" "}
                        <br />
                        <Link
                          to={(loc) => ({
                            ...loc,
                            pathname: `${loc.pathname}/edit`,
                            state: {
                              prefil: data,
                            },
                          })}
                        >
                          <Button>Edit</Button>
                        </Link>
                      </Row>
                    </>
                  );
                }}
              />
            ),
            subs: [
              {
                name: "Edit",
                url: "/edit",
                hideInSubs: true,
                hidden: "loggedin",
                restrict: "loggedin",
                comp: (
                  <CustomForm
                    title="Edit User"
                    render={(bag, props) => {
                      return (
                        <>
                          {props.inputs.map((input) => {
                            return (
                              <CustomInput
                                {...input}
                                bag={bag}
                                key={JSON.stringify(input)}
                              />
                            );
                          })}{" "}
                          <div style={{ marginTop: "10px" }}>
                            <Button
                              style={{ marginRight: "10px" }}
                              type="submit"
                              disabled={bag.isSubmitting}
                            >
                              Submit
                            </Button>
                            <Button type="button" onClick={bag.handleReset}>
                              Reset
                            </Button>
                          </div>
                        </>
                      );
                    }}
                    auth={() => this.getAuth()}
                    inputs={[
                      {
                        label: "ID",
                        name: "_id",
                        text: "text",
                        placeholder: "ID",
                        disabled: true,
                      },
                      {
                        label: "First Name",
                        name: "first_name",
                        type: "text",
                        placeholder: "Jane",
                      },
                      {
                        label: "Last Name",
                        name: "last_name",
                        type: "text",
                        placeholder: "Doe",
                      },
                      {
                        label: "Email Address",
                        name: "email",
                        type: "email",
                        placeholder: "jane@email.com",
                      },
                      {
                        label: "Password",
                        name: "password",
                        type: "password",
                        disabled: true,
                        placeholder: "",
                      },
                      {
                        label: "Phone number",
                        name: "phone_no",
                        type: "text",
                        placeholder: "07900000000",
                      },
                      {
                        label: "NHS Number",
                        name: "nhs_number",
                        type: "text",
                        placeholder: "000-000-0000",
                      },
                      {
                        label: "Last Blood Test",
                        name: "last_blood_test",
                        type: "datetime",
                        placeholder: "000-000-0000",
                      },
                      {
                        label: "Reason",
                        name: "reason",
                        type: "text",
                        disabled: true,
                      },
                    ]}
                    url="http://ntukfc.me/api/account/staff/change_profile/:email"
                    submit={(values, props, { setSubmitting, setStatus }) => {
                      this.state.auth
                        .makeReq(
                          true,
                          `http://ntukfc.me/api/account/staff/change_profile/${props.match.params.id}`,
                          "PATCH",
                          values
                        )
                        .then((data) => {
                          setSubmitting(false);
                          setStatus(data);
                        })
                        .catch((error) => {
                          setSubmitting(false);
                          setStatus(error);
                        });
                    }}
                  />
                ),
              },
            ],
          },
        ],
      },
      {
        name: "Medications",
        url: "/meds",
        restrict: "loggedin",
        hideInSubs: true,
        perms: ["medication.view"],
        comp: (
          <CustomList
            auth={() => this.getAuth()}
            render={(data, noData) => {
              console.log(data);
              if (data.status !== 200 || data.medications.length < 1) {
                return noData();
              } else {
                return (
                  <Table>
                    <thead>
                      <th>ID</th>
                      <th>Name</th>
                      <th>Bloodwork Required</th>
                    </thead>
                    <tbody>
                      {data.medications.map((med) => {
                        return (
                          <tr key={JSON.stringify(med)}>
                            <td>{med._id}</td>
                            <td>{capitalize(med.med_name)}</td>
                            <td>
                              {med.med_blood_work === 0
                                ? "Not required"
                                : med.med_blood_work === 1
                                ? "Required at a later date"
                                : "Required"}
                            </td>
                          </tr>
                        );
                      })}
                    </tbody>
                  </Table>
                );
              }
            }}
            title="Medications"
            url={`http://ntukfc.me/api/medication/`}
          />
        ),
      },
      {
        name: "request",
        url: "/request",
        hidden: "loggedin",
        restrict: "notloggedin",
        comp: (
          <CustomForm
            title="Access Request"
            centerTitle={true}
            render={(bag, props) => {
              return (
                <div>
                  <Row md={{ span: 1, offset: 6 }}>
                    <Col md={{ span: 4, offset: 4 }}>
                      {props.inputs.map((input) => {
                        return (
                          <CustomInput
                            {...input}
                            bag={bag}
                            placeholder={input.placeholder}
                            disabled={input.disabled ? input.disabled : false}
                          />
                        );
                      })}{" "}
                    </Col>
                  </Row>
                  <Row className="text-center mt-5">
                    <Col md={{ span: 1, offset: 5 }}>
                      <Button type="submit" disabled={bag.isSubmitting}>
                        Submit
                      </Button>
                    </Col>
                    <Col md={{ span: 1, offset: 0 }}>
                      <Link to="/login">
                        <Button variant="secondary">Login</Button>
                      </Link>
                    </Col>
                  </Row>
                </div>
              );
            }}
            auth={() => this.getAuth()}
            url="http://ntukfc.me/api/auth/request_access"
            submit={(values, props, { setSubmitting, setStatus, ...rest }) => {
              this.getAuth()
                .makeReq(
                  false,
                  `http://ntukfc.me/api/auth/request_access/`,
                  "POST",
                  values
                )
                .then((data) => {
                  setSubmitting(false);
                  setStatus(data);
                })
                .catch((error) => {
                  setSubmitting(false);
                  setStatus(error);
                });
            }}
            initial={{
              first_name: "",
              last_name: "",
              email: "",
              phone_no: "",
              nhs_number: "",
              password: "",
              reason: "",
            }}
            inputs={[
              {
                label: "First Name",
                name: "first_name",
                type: "text",
                placeholder: "Jane",
              },
              {
                label: "Last Name",
                name: "last_name",
                type: "text",
                placeholder: "Doe",
              },
              {
                label: "Email Address",
                name: "email",
                type: "email",
                placeholder: "jane@email.com",
              },
              {
                label: "Password",
                name: "password",
                type: "password",
                placeholder: "",
              },
              {
                label: "Phone number",
                name: "phone_no",
                type: "text",
                placeholder: "07900000000",
              },
              {
                label: "NHS Number",
                name: "nhs_number",
                type: "text",
                placeholder: "000-000-0000",
              },
              {
                label: "Reason",
                name: "reason",
                type: "text",
              },
            ]}
            schema={Yup.object({
              first_name: Yup.string()
                .max(15, "Must be 15 characters or less")
                .required("Required"),
              last_name: Yup.string()
                .max(20, "Must be 20 characters or less")
                .required("Required"),
              email: Yup.string()
                .email("Invalid email addresss`")
                .required("Required"),
              phone_no: Yup.string()
                .matches(
                  /^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$/,
                  "Invalid phone number"
                )
                .required("Required"),
              password: Yup.string().required("Required"),
              nhs_number: Yup.string()
                .matches(/^\d{3}\s?\-?\d{3}\s?\-?\d{4}$/, "Invalid NHS number")
                .required("Required"),
              reason: Yup.string().required("Required"),
            })}
          />
        ),
      },
      {
        name: "login",
        url: "/login",
        hidden: "loggedin",
        restrict: "notloggedin",
        comp: (
          <CustomForm
            render={(bag, props) => {
              return (
                <div style={{ paddingTop: "10vh" }}>
                  <Media>
                    <img
                      style={{ marginLeft: "auto", marginRight: "auto" }}
                      width="500px"
                      src="https://cdn.discordapp.com/attachments/763434469104549929/816884127596544011/Untitled-1.png"
                    />
                  </Media>
                  <Row md={{ span: 1, offset: 6 }}>
                    <Col md={{ span: 2, offset: 5 }}>
                      {props.inputs.map((input) => {
                        return (
                          <CustomInput
                            {...input}
                            bag={bag}
                            key={JSON.stringify(input)}
                          />
                        );
                      })}
                    </Col>
                  </Row>
                  <Row className="text-center mt-5">
                    <Col md={{ span: 1, offset: 5 }}>
                      <Button type="submit" disabled={bag.isSubmitting}>
                        Submit
                      </Button>
                    </Col>
                    <Col md={{ span: 1, offset: 0 }}>
                      <Link to="/request">
                        <Button variant="secondary">Register</Button>
                      </Link>
                    </Col>
                  </Row>
                </div>
              );
            }}
            schema={Yup.object({
              email: Yup.string()
                .email("Invalid email addresss`")
                .required("Required"),
              password: Yup.string().required("Required"),
            })}
            auth={() => this.getAuth()}
            initial={{
              email: "",
              password: "",
            }}
            submit={(values, props, { setSubmitting, setStatus }) => {
              this.state.auth
                .login(values)
                .then((data) => {
                  setSubmitting(false);
                  props.history.push("/myinfo");
                  setStatus(data);
                })
                .catch((error) => {
                  setSubmitting(false);
                  setStatus(error);
                });
            }}
            inputs={[
              {
                label: "Email Address",
                name: "email",
                type: "email",
                placeholder: "jane@email.com",
              },
              {
                label: "Password",
                name: "password",
                type: "password",
                placeholder: "",
              },
            ]}
          />
        ),
      },
      {
        name: "My Info",
        url: "/myinfo",
        restrict: "loggedin",
        hideInSubs: true,
        comp: (
          <CustomView
            auth={() => this.getAuth()}
            title="My Information"
            url="http://ntukfc.me/api/account/"
            render={(data) => {
              data = data.user;
              let lastBlood = new Date(data.last_blood_test);
              return (
                <>
                  <Col>
                    <b>ID: </b> <p>{data._id}</p>
                    <b>First Name: </b> <p>{capitalize(data.first_name)}</p>
                    <b>Last Name: </b> <p>{capitalize(data.last_name)}</p>
                    <b>Email: </b> <p>{data.email}</p>
                    <b>Password: </b>
                    <p>
                      <Link to={"/myinfo/changemypass"}>Change</Link>
                    </p>
                    <b>Phone Number: </b> <p>{data.phone_no}</p>
                    <b>NHS Number: </b> <p>{data.nhs_number}</p>
                    <b>Last Blood Test: </b>{" "}
                    <p>
                      {lastBlood.getFullYear() === 1970
                        ? "Not assigned"
                        : `${lastBlood.getDate()}/${
                            lastBlood.getMonth() + 1
                          }/${lastBlood.getFullYear()} @ ${(
                            "0" + lastBlood.getHours()
                          ).slice(-2)}:${("0" + lastBlood.getMinutes()).slice(
                            -2
                          )}`}
                    </p>
                  </Col>
                </>
              );
            }}
          />
        ),
        subs: [
          {
            name: "Change My Password",
            url: "/changemypass",
            hidden: "loggedin",
            restrict: "loggedin",
            comp: (
              <>
                <CustomForm
                  title="Change Password"
                  centerTitle={true}
                  auth={() => {
                    this.getAuth();
                  }}
                  url="http://ntukfc.me/api/account/change_password"
                  inputs={[
                    {
                      label: "Old Password",
                      type: "password",
                      name: "password_old",
                    },
                    {
                      label: "New Password",
                      type: "password",
                      name: "password",
                    },
                    {
                      label: "New Password Again",
                      type: "password",
                      name: "password_confirm",
                    },
                  ]}
                  schema={Yup.object({
                    password: Yup.string().required("Required"),
                    password_confirm: Yup.string().required("Required"),
                    password_old: Yup.string().required("Required"),
                  })}
                  submit={(
                    values,
                    props,
                    { setSubmitting, setStatus, ...rest }
                  ) => {
                    this.getAuth()
                      .makeReq(
                        true,
                        "http://ntukfc.me/api/account/change_password",
                        "PATCH",
                        values
                      )
                      .then((data) => {
                        setStatus(data);
                        setSubmitting(false);
                      })
                      .catch((error) => {
                        setStatus(error);
                        setSubmitting(false);
                      });
                  }}
                  render={(bag, props) => {
                    return (
                      <>
                        <Col md={{ offset: 4, span: 4 }}>
                          {props.inputs.map((input) => {
                            return (
                              <CustomInput
                                {...input}
                                bag={bag}
                                key={JSON.stringify(input)}
                              />
                            );
                          })}
                          <div style={{ marginTop: "30px" }}>
                            <Button type="submit" disabled={bag.isSubmitting}>
                              Submit
                            </Button>
                          </div>
                        </Col>
                      </>
                    );
                  }}
                />
              </>
            ),
          },
        ],
      },
      {
        name: "Logout",
        url: "/logout",
        perms: ["user.logout"],
        onClick: () => this.state.auth.logout(),
        comp: <></>,
      },
      {
        name: "defredir",
        url: "/",
        hidden: "always",
        comp: (
          <Redirect
            to={{
              pathname: "/myinfo",
            }}
          />
        ),
      },
    ];

    this.setAuthed = this.setAuthed.bind(this);
    this.setLoading = this.setLoading.bind(this);
    this.getAuth = this.getAuth.bind(this);
  }
  componentDidMount() {
    this.state.auth
      .restoreSession()
      .then(() => {
        this.setState({
          loading: false,
          auth: { ...this.state.auth, isAuthenticated: true },
        });
      })
      .catch((error) => {
        console.log(`ERROR: componentDidMount: ${error}`);
        this.setState({ loading: false, auth: { ...this.state.auth } });
      });
  }
  render() {
    return (
      <>
        {this.state.loading ? (
          <h1>Loading, please wait...</h1>
        ) : (
          <Container fluid>
            <Router>
              <Main
                auth={() => this.getAuth()}
                setAuthed={this.setAuthed}
                setLoading={this.setLoading}
                routes={this.routes}
              />
              {/*               
              {this.state.auth.isAuthenticated ? (
                
              ) : (
                <Login
                  auth={this.state.auth}
                  setAuthed={this.setAuthed}
                  setLoading={this.setLoading}
                  routes={this.routes}
                />
              )} */}
            </Router>
          </Container>
        )}
      </>
    );
  }
}

export default App;
