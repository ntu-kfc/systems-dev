const mongoose = require('mongoose');
const medSchema = new mongoose.Schema({
    med_name: {
        type: String,
        required: true
    },
    med_blood_work: {
        type: Number,
        required: true,
    },
    med_blood_work_days_without: {
        type: Number,
        required: false,
    }
});
module.exports = mongoose.model('Medication', medSchema);
