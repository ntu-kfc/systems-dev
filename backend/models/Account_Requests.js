const mongoose = require('mongoose');
const accountRequestsSchema = new mongoose.Schema({
    first_name: {
        type: String,
        required: true,
        max: 20,
        min: 2
    },
    last_name: {
        type: String,
        required: true,
        max: 20,
        min: 2
    },
    email: {
        type: String,
        required: true,
        max: 150,
        min: 5
    },
    phone_no: {
        type: String,
        required: true,
        max: 20,
        min: 10
    },    
    password: {
        type: String,
        required: true,
        max: 1024,
        min:6
    }, 
    reason: {
        type: String,
        required: true,
        max: 30,
        min: 1000
    }, 
    nhs_number: {
        type: String,
        required: true
    }
});
module.exports = mongoose.model('Account_Requests', accountRequestsSchema);
