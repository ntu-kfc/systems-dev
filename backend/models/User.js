const mongoose = require('mongoose');
const userSchema = new mongoose.Schema({
    first_name: {
        type: String,
        required: true,
        max: 20,
        min: 2
    },
    last_name: {
        type: String,
        required: true,
        max: 20,
        min: 2
    },
    email: {
        type: String,
        required: true,
        max: 150,
        min: 5
    },
    phone_no: {
        type: String,
        required: true,
        max: 20,
        min: 10
    },    
    password: {
        type: String,
        required: true,
        max: 1024,
        min:6
    },
    groups: {
        type: Array,
        required: true,
        default: ['patient']
    },
    nhs_number: {
        type: String,
        required: true
    },
    last_blood_test: {
        type: Date,
        required: false,
        default: new Date(+0)
    }
});
module.exports = mongoose.model('User', userSchema);
