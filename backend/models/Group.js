const mongoose = require('mongoose');
const groupsSchema = new mongoose.Schema({
    group_name: {
        type: String,
        required: true
    },
    group_perms: {
        type: Array,
        required: true,
    }
});
module.exports = mongoose.model('Group', groupsSchema);
