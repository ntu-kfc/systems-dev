const mongoose = require('mongoose');
const { DayPage } = require('twilio/lib/rest/bulkexports/v1/export/day');
const perscriptionSchema = new mongoose.Schema({
    patient_email: {
        type: String,
        required: true
    },
    medication_id: {
        type: String,
        required: true,
    },
    medication_data: {
        type: Array,
        required: false,
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    collection_time: {
        type: Date,
        default: null
    },
    bloodtest_needed: {
        type: Boolean,
        required: true,
        default: false
    },
    bloodtest_completed: {
        type: Boolean,
        required: false
    },
    collected: {
        type: Boolean,
        default: false
    },

});
module.exports = mongoose.model('Perscription', perscriptionSchema);
