const jwt = require('jsonwebtoken');

module.exports = function(req,res,next) {
    const token = req.header('auth-token');
    if(!token)  return res.status(401).json({error: "No JWT token found.", "status":401});

    try {
        const verifed = jwt.verify(token, process.env.TOKEN_SECRET);
        req.user = verifed;
        next();
    }catch(err){
        res.status(400).json({error: "Invalid JWT token." , "status":400});
    }
}