const router = require('express').Router();
const Medication = require('../models/Medication');
const Perscription = require('../models/Perscription');
const verify = require('./verifyToken');
const User = require('../models/User');
const jwt = require ('jsonwebtoken');
const bcrypt = require('bcryptjs');
const dotenv = require('dotenv');
dotenv.config();
const twilio = require('twilio')(process.env.TWILIO_ACCOUNT_SID, process.env.TWILIO_AUTH_TOKEN);


router.patch('/edit/:perscription', verify, async(req, res) => {


    const auth_token = req.headers["auth-token"];
    jwt.verify(auth_token, process.env.TOKEN_SECRET, async function(err, decoded) { 
        if (err) return res.status(400).json({"error":"corrupted token", "status":400});
 
        const groups = decoded.groups;
        if (groups.includes("admin") || groups.includes("pharmacist") || groups.includes("technician")) {

            //Checks to see if the users email is already in the database
            if (req.params.perscription.match(/^[0-9a-fA-F]{24}$/)) {
                const perExists = await Perscription.exists({_id: req.params.perscription});
                if(!perExists) 
                    return res.status(400).json({"error":"No Perscription Found By That ID", "status":400});
            } else {
                return res.status(400).json({"error":"Invalid Perscription ID", "status":400});
            }

            // Checks if is valid User
            const emailExists = await User.findOne({email: req.body.patient_email});
            if(!emailExists) 
                return res.status(400).json({"error":"email not found!", "status":400});

            //Checks to see if any medication by that ID exists in the database
            if (req.body.medication_id.match(/^[0-9a-fA-F]{24}$/)) {
                const medicationExists = await Medication.exists({_id: req.body.medication_id});
                if(!medicationExists) 
                    return res.status(400).json({"error":"No Medication Found By That ID", "status":400});
            } else {
                return res.status(400).json({"error":"Invalid Medication ID", "status":400});
            }

            const med = await Medication.findOne({_id: req.body.medication_id});
            twilio.messages.create({body: `Hey ${emailExists.first_name}, something has changed with one of your prescriptions! Collection Time: ${req.body.collection_time}\nCollected: ${req.body.collected ? "Yes" : "No"}\nMedication: ${med.med_name}`, from: '+447782567214', to: emailExists.phone_no}).then(message => res.json({"data":"success","status":200,"sms": message}));


            Perscription.findOneAndUpdate({_id: req.params.perscription}, { 
                patient_email: req.body.patient_email,
                medication_id: req.body.medication_id,
                collection_time: req.body.collection_time,
                bloodtest_completed: req.body.bloodtest_completed,
                collected: req.body.collected
                }, function(err, result) {
                if (err) {
                    return res.status(400).json({"error":"Couldn't edit perscription", "status":400});
                } else {
                    return res.json( {"data":"success","status":200,"message":"Data Updated!"});
                }
            });
        } else {
            res.status(403).json({"data":"Unauthorized", "status":403})
        }
    });
});

router.post('/new', verify, async (req, res) => {
    console.log('[Information] Incoming Post Request New Perscription');

    const auth_token = req.headers["auth-token"];
    jwt.verify(auth_token, process.env.TOKEN_SECRET, async function(err, decoded) { 
        if (err) return res.status(400).json({"error":"corrupted token", "status":400});
 
        const groups = decoded.groups;
        if (groups.includes("admin") || groups.includes("pharmacist")  || groups.includes("technician")) {
            // Makes sure correct info is sent to PAI
            if (!req.body.patient_email || !req.body.medication_id) 
                return res.status(400).json({"error":"Invalid Request. Requires: patient_email, medication_id", "status":400});

            // Checks if is valid User
            const emailExists = await User.findOne({email: req.body.patient_email});
            if(!emailExists) 
                return res.status(400).json({"error":"email not found!", "status":400});

            //Checks to see if any medication by that ID exists in the database
            if (req.body.medication_id.match(/^[0-9a-fA-F]{24}$/)) {
            const medicationExists = await Medication.exists({_id: req.body.medication_id});
            if(!medicationExists) 
                return res.status(400).json({"error":"No Medication Found By That ID", "status":400});
            } else {
                return res.status(400).json({"error":"Invalid Medication ID", "status":400});
            }

            // Lets check if the user requires bloodwork before we can give them medication then send them an SMS....
            const medication = await Medication.findOne({_id: req.body.medication_id});
            const user = await User.findOne({email: req.body.patient_email});

            var timestamp = new Date().getTime() - (medication.med_blood_work_days_without * 24 * 60 * 60 * 1000);

            if (medication.med_blood_work == 2 && user.last_blood_test < timestamp) {
            twilio.messages.create({body: `Hey ${user.first_name}, we can't allow you to pickup your perscription item (${medication.med_name}) untill you have had a bloodtest! Please give your local GP a call to arrange this.`, from: '+447782567214', to: user.phone_no}).then(message => res.json({"data":"success","status":200,"sms": message}));
            const per = new Perscription({
                patient_email: req.body.patient_email,
                medication_id: req.body.medication_id,
                bloodtest_needed: true
            }).save();
            } else if(medication.med_blood_work == 1 && user.last_blood_test < timestamp){
                //Work out a time for pickup tomorrow
                let hour = Math.floor(Math.random()*(16-9+1)+9);
                let min = Math.round((Math.random()*(60-0)+0)/5)*5;
                twilio.messages.create({body: `Hey ${user.first_name}, your perscription item (${medication.med_name}) is ready for pickup! Your suggested pick up time is Tomorrow at ${hour}:${min} Please reply with either "Y" if this time suites you or "N" if it doesn't. Please be warned you are required to contact your GP for a bloodtest while taking this perscription!`, from: '+447782567214', to: user.phone_no}).then(message => res.json({"data":"success","status":200,"sms": message}));
                var bookDate  = new Date();
                bookDate.setDate(bookDate.getDate() + 1)
                bookDate.setHours(hour);
                bookDate.setMinutes(min);
                const per = new Perscription({
                    patient_email: req.body.patient_email,
                    medication_id: req.body.medication_id,
                    collection_time: bookDate,
                    bloodtest_needed: true
                }).save();
            } else {
                //Work out a time for pickup tomorrow
                let hour = Math.floor(Math.random()*(16-9+1)+9);
                let min = Math.round((Math.random()*(60-0)+0)/5)*5;
                twilio.messages.create({body: `Hey ${user.first_name}, your perscription item (${medication.med_name}) is ready for pickup! Your suggested pick up time is Tomorrow at ${hour}:${min} Please reply with either "Y" if this time suites you or "N" if it doesn't.`, from: '+447782567214', to: user.phone_no}).then(message => res.json({"data":"success","status":200,"sms": message}));
                var bookDate  = new Date();
                bookDate.setDate(bookDate.getDate() + 1)
                bookDate.setHours(hour);
                bookDate.setMinutes(min);
                const per = new Perscription({
                    patient_email: req.body.patient_email,
                    medication_id: req.body.medication_id,
                    collection_time: bookDate
                }).save();
            }
            res.json( {"data":"success","status":200,"message":"Perscription has been added to the database"});
        } else {
            res.status(403).json({"data":"Unauthorized", "status":403})
        }
    });

});

router.get('/', verify, async(req, res) => {
    const auth_token = req.headers["auth-token"];
    jwt.verify(auth_token, process.env.TOKEN_SECRET, async function(err, decoded) { 
        if (err) return res.status(400).json({"error":"corrupted token", "status":400});

        const groups = decoded.groups;
        if (groups.includes("admin") || groups.includes("pharmacist") || groups.includes("technician")) {
            const perscription = await Perscription.find({}).select('-__v');
            for (per in perscription) {
               const medication = await Medication.findOne({_id: perscription[per].medication_id}).select('-__v').select('-_id');
       
               const user = await User.findOne({email: perscription[per].patient_email});
               const timestamp = new Date().getTime() - (medication.med_blood_work_days_without * 24 * 60 * 60 * 1000);
       
               if((medication.med_blood_work == 2  || medication.med_blood_work == 1) && (user.last_blood_test < timestamp)) {
                   perscription[per].bloodtest_needed = true;
               } else {
                   perscription[per].bloodtest_needed = false;
               }
       
               perscription[per].medication_data = medication;
            }
            res.json({"data":"success","status":200, "size": perscription.length,"perscriptions": perscription});
           
         } else {
             res.status(403).json({"data":"Unauthorized", "status":403})
         }

    });
});

router.get('/current_user', verify, async(req, res) => {
    const auth_token = req.headers["auth-token"];
    jwt.verify(auth_token, process.env.TOKEN_SECRET, async function(err, decoded) { 
        if (err) return res.status(400).json({"error":"corrupted token", "status":400});

            const emailExists = await User.findOne({email: decoded.email});
            if(!emailExists) 
               return res.status(400).json({"error":"email not found!", "status":400});
        
            const perscription = await Perscription.find({patient_email: decoded.email}).select('-__v');
            for (per in perscription) {
               const medication = await Medication.findOne({_id: perscription[per].medication_id}).select('-__v').select('-_id');
        
               const user = await User.findOne({email: perscription[per].patient_email});
               const timestamp = new Date().getTime() - (medication.med_blood_work_days_without * 24 * 60 * 60 * 1000);
        
               if((medication.med_blood_work == 2  || medication.med_blood_work == 1) && (user.last_blood_test < timestamp)) {
                   perscription[per].bloodtest_needed = true;
               } else {
                   perscription[per].bloodtest_needed = false;
               }
        
               perscription[per].medication_data = medication;
            }
            res.json({"data":"success","status":200, "size": perscription.length,"perscriptions": perscription});
      
    });
});


router.get('/user/:email', verify, async(req, res) => {
    const auth_token = req.headers["auth-token"];
    jwt.verify(auth_token, process.env.TOKEN_SECRET, async function(err, decoded) { 
        if (err) return res.status(400).json({"error":"corrupted token", "status":400});

        const groups = decoded.groups;
        if (groups.includes("admin") || groups.includes("pharmacist") || groups.includes("technician")) { 
            const emailExists = await User.findOne({email: req.params.email});
            if(!emailExists) 
               return res.status(400).json({"error":"email not found!", "status":400});
        
            const perscription = await Perscription.find({patient_email: req.params.email}).select('-__v');
            for (per in perscription) {
               const medication = await Medication.findOne({_id: perscription[per].medication_id}).select('-__v').select('-_id');
        
               const user = await User.findOne({email: perscription[per].patient_email});
               const timestamp = new Date().getTime() - (medication.med_blood_work_days_without * 24 * 60 * 60 * 1000);
        
               if((medication.med_blood_work == 2  || medication.med_blood_work == 1) && (user.last_blood_test < timestamp)) {
                   perscription[per].bloodtest_needed = true;
               } else {
                   perscription[per].bloodtest_needed = false;
               }
        
               perscription[per].medication_data = medication;
            }
            res.json({"data":"success","status":200, "size": perscription.length,"perscriptions": perscription});
           
        } else {
            res.status(403).json({"data":"Unauthorized", "status":403})
        } 
    
    });
});

router.get('/:perscriptionId', verify, async(req, res) => {
    //Checks to see if the users email is already in the database
    if (req.params.perscriptionId.match(/^[0-9a-fA-F]{24}$/)) {
        const perExists = await Perscription.exists({_id: req.params.perscriptionId});
        if(!perExists) 
            return res.status(400).json({"error":"No Perscription Found By That ID", "status":400});
    } else {
        return res.status(400).json({"error":"Invalid Perscription ID", "status":400});
    }

    const per = await Perscription.findOne({_id: req.params.perscriptionId}).select('-__v');;
    const medication = await Medication.findOne({_id: per.medication_id}).select('-__v').select('-_id');
    const user = await User.findOne({email: per.patient_email});
    const timestamp = new Date().getTime() - (medication.med_blood_work_days_without * 24 * 60 * 60 * 1000);

    if((medication.med_blood_work == 2  || medication.med_blood_work == 1) && (user.last_blood_test < timestamp)) {
        per.bloodtest_needed = true;
    } else {
        per.bloodtest_needed = false;
    }

    per.medication_data = medication;
    
    res.json({"data":"success","status":200,"perscription": per});
   
});


router.get('/user/bloodtests/:email', verify, async(req, res) => {
    const emailExists = await User.findOne({email: req.params.email});
    if(!emailExists) 
       return res.status(400).json({"error":"email not found!", "status":400});

    const perscription = await Perscription.find({patient_email: req.params.email, bloodtest_needed: true}).select('-__v');
    for (per in perscription) {
        const medication = await Medication.findOne({_id: perscription[per].medication_id}).select('-__v').select('-_id');

        const user = await User.findOne({email: perscription[per].patient_email});
        const timestamp = new Date().getTime() - (medication.med_blood_work_days_without * 24 * 60 * 60 * 1000);

        if((medication.med_blood_work == 2  || medication.med_blood_work == 1) && (user.last_blood_test < timestamp)) {
            perscription[per].bloodtest_needed = true;
        } else {
            perscription[per].bloodtest_needed = false;
        }

        if (medication)
            perscription[per].medication_data = medication;
        else
            delete perscription[per];
    }
    res.json({"data":"success","status":200, "size": perscription.length,"perscriptions": perscription});
   
});


module.exports = router;