const router = require('express').Router();
const verify = require('./verifyToken');
const User = require('../models/User');
const Account_Requests = require('../models/Account_Requests');
const Group = require('../models/Group');
const jwt = require ('jsonwebtoken');
const bcrypt = require('bcryptjs');
const dotenv = require('dotenv');
dotenv.config();
const twilio = require('twilio')(process.env.TWILIO_ACCOUNT_SID, process.env.TWILIO_AUTH_TOKEN);

router.get('/staff/account_requests', verify, async(req, res) => {

     const auth_token = req.headers["auth-token"];
     jwt.verify(auth_token, process.env.TOKEN_SECRET, async function(err, decoded) { 
         if (err) return res.status(400).json({"error":"corrupted token", "status":400});
 
         const groups = decoded.groups;
         if (groups.includes("admin") || groups.includes("pharmacist")) {
            Account_Requests.find({}, function(err, requests) {
                res.json({"data":"success","status":200, "size": requests.length,"requests": requests});
             });
         } else {
             res.status(403).json({"data":"Unauthorized", "status":403})
         }
     });
});

router.get('/staff/account_request/:email', verify, async(req, res) => {

    const auth_token = req.headers["auth-token"];
    jwt.verify(auth_token, process.env.TOKEN_SECRET, async function(err, decoded) { 
        if (err) return res.status(400).json({"error":"corrupted token", "status":400});

        const groups = decoded.groups;
        if (groups.includes("admin") || groups.includes("pharmacist")) {

            const emailExists = await Account_Requests.findOne({email: req.params.email});
            if(!emailExists) 
               return res.status(400).json({"error":"email not found!", "status":400});
        

            Account_Requests.findOne({email: req.params.email} , function(err, requests) {
                res.json({"data":"success","status":200,"request": requests});
             });
        } else {
            res.status(403).json({"data":"Unauthorized", "status":403})
        }
    });
});


router.get('/', verify, async(req, res) => {
    const auth_token = req.headers["auth-token"];
    jwt.verify(auth_token, process.env.TOKEN_SECRET, async function(err, decoded) {
        if (err) return res.status(400).json({"error":"corrupted token", "status":400});

        const emailExists = await User.findOne({email: decoded.email});
        if(!emailExists) return res.status(400).json({"error":"email not found!", "status":400});

        User.findOne({email: decoded.email}, function(err, user) {
            res.json({"data":"success","status":200,"user": user});
        });
      });
});

router.patch('/change_password', verify, async(req, res) => {

    if (!req.body.password || !req.body.password_old || !req.body.password_confirm) 
    return res.status(400).json({"error":"Invalid Request. Requires: password, password_old, password_confirm", "status":400});

    const auth_token = req.headers["auth-token"];
    jwt.verify(auth_token, process.env.TOKEN_SECRET, async function(err, decoded) {
        if (err) return res.status(400).json({"error":"corrupted token", "status":400});

        const user = await User.findOne({email: decoded.email});
        if(!user) return res.status(400).json({"error":"email not found!", "status":400});

        const validPassword = await bcrypt.compare(req.body.password_old, user.password);
        if(!validPassword) return res.status(401).json({"error":"wrong_password", "status":400,"title":"wrong_password", "error_description": "Invalid password combination"});

        if (req.body.password != req.body.password_confirm) return res.status(400).json({"error":"password_mismatch", "status":40,"title":"Password Mismatch", "error_description": "Passwords do not match"});

        const salt = await bcrypt.genSalt(10);
        const hashedPassword = await bcrypt.hash(req.body.password, salt);

        User.findOneAndUpdate({email: decoded.email}, { 
            password: hashedPassword
            }, function(err, result) {
            if (err) {
                return res.status(400).json({"error":"Couldn't edit user with that email!", "status":400});
            } else {
                return res.json( {"data":"success","status":200,"message":"Users passworrd has been updated!"});
            }
          });
      });
});

router.get('/staff/accounts', verify, async(req, res) => {
    const auth_token = req.headers["auth-token"];
    jwt.verify(auth_token, process.env.TOKEN_SECRET, async function(err, decoded) {
        if (err) return res.status(400).json({"error":"corrupted token", "status":400});
        const groups = decoded.groups;

        if (groups.includes("admin") || groups.includes("pharmacist")) {
            User.find({}, function(err, users) {
                res.json({"data":"success","status":200, "size": users.length,"users": users});
             });
        } else {
            res.status(403).json({"data":"Unauthorized", "status":403})
        }
    });
});

router.get('/staff/accounts/:email', verify, async(req, res) => {
    const auth_token = req.headers["auth-token"];
    jwt.verify(auth_token, process.env.TOKEN_SECRET, async function(err, decoded) {
        if (err) return res.status(400).json({"error":"corrupted token", "status":400});

        const emailExists = await User.findOne({email: req.params.email});
        if(!emailExists) 
           return res.status(400).json({"error":"email not found!", "status":400});
    
        const groups = decoded.groups;
        console.log(groups)
        if (groups.includes("admin") || groups.includes("pharmacist")) {
            User.findOne({email: req.params.email} , function(err, user) {
                res.json({"data":"success","status":200,"user": user});
             });
        } else {
            res.status(403).json({"data":"Unauthorized", "status":403})
        }
    });
});

router.patch('/staff/change_profile/:email', verify, async(req, res) => {

    const auth_token = req.headers["auth-token"];
    jwt.verify(auth_token, process.env.TOKEN_SECRET, async function(err, decoded) {
        if (err) return res.status(400).json({"error":"corrupted token", "status":400});
        if (!req.body.email || !req.body.phone_no || !req.body.first_name || !req.body.last_name) 
        return res.status(400).json({"error":"Invalid Request. Requires: first_name, last_name, email, phone_no, groups(array), nhs_number, last_blood_test", "status":400});

        //Checks to see if the users email is already in the database
        const emailExists = await User.findOne({email: req.params.email});
        if(!emailExists) 
        return res.status(400).json({"error":"Account not found!", "status":400});

        if (!req.body.groups) 
            return res.status(400).json({"error":"Invalid Request. Requires: groups(array)", "status":400});

        for (i = 0; i < req.body.groups.length; i++) {
            const groupExists = await Group.findOne({group_name: req.body.groups[i]});
            if (!groupExists)
                return res.status(400).json({"error":`You are trying to assign groups that do not exist: ${req.body.groups[i]}`, "status":400});
        }
        const groups = decoded.groups;
        console.log(groups)
        if (groups.includes("admin") || groups.includes("pharmacist")) {
            User.findOneAndUpdate({email: req.params.email}, { 
                first_name: req.body.first_name,
                last_name: req.body.last_name,
                email: req.body.email,
                phone_no: req.body.phone_no,
                groups: req.body.groups,
                nhs_number: req.body.nhs_number,
                last_blood_test: req.body.last_blood_test
                }, function(err, result) {
                if (err) {
                    return res.status(400).json({"error":"Couldn't edit user with that email!", "status":400});
                } else {
                    return res.json( {"data":"success","status":200,"message":"Data Updated!"});
                }
            });   
        } else {
            res.status(403).json({"data":"Unauthorized", "status":403})
        }
    });
});

//Non autistic remove access function
router.post('/staff/deny_access/:email', verify, async (req, res) => {
    console.log('[Information] Incoming Post Request Denying Access');

    const auth_token = req.headers["auth-token"];
    jwt.verify(auth_token, process.env.TOKEN_SECRET, async function(err, decoded) {
        if (err) return res.status(400).json({"error":"corrupted token", "status":400});

        const groups = decoded.groups;
        if (groups.includes("admin") || groups.includes("pharmacist")) {

            const application = await Account_Requests.findOne({email: req.params.email});
            if(!application) 
                return res.status(400).json({"error":"Application not found.", "status":400});   
        
            try {
                application.deleteOne({email: req.params.email}, function(err, result) {  return res.json( {"data":"success","status":200,"message":"Application Denied"});});
                //Sends the SMS
                twilio.messages.create({body: `Hey ${application.first_name}, we are just messaging you to let you know your account has been denied. This could be for many reasons. Feel free to reapply at any time!`, from: '+447782567214', to: application.phone_no});
            } catch (err) {
                res.status(400).send(err);
            }
        } else {
            res.status(403).json({"data":"Unauthorized", "status":403})
        } 
    });  
});


//Non autistic grant access function
router.post('/staff/grant_access/:email', verify, async (req, res) => {
    console.log('[Information] Incoming Post Request Granting Access');
    const auth_token = req.headers["auth-token"];
    jwt.verify(auth_token, process.env.TOKEN_SECRET, async function(err, decoded) {
        if (err) return res.status(400).json({"error":"corrupted token", "status":400});
        const application = await Account_Requests.findOne({email: req.params.email});
        const groups = decoded.groups;
        if (groups.includes("admin") || groups.includes("pharmacist")) {
            if(!application) 
            return res.status(400).json({"error":"Application not found.", "status":400});

            if (!req.body.groups) 
                return res.status(400).json({"error":"Invalid Request. Requires: groups(array)", "status":400});

                for (i = 0; i < req.body.groups.length; i++) {
                    const groupExists = await Group.findOne({group_name: req.body.groups[i]});
                    if (!groupExists)
                        return res.status(400).json({"error":`You are trying to assign groups that do not exist: ${req.body.groups[i]}`, "status":400});
                }


            // Now lets create a new instance of a user 
            const user = new User({
                first_name: application.first_name,
                last_name: application.last_name,
                email: application.email,
                phone_no: application.phone_no,
                password: application.password,
                groups: req.body.groups,
                nhs_number: application.nhs_number
            });


            try {
                const savedUser = await user.save();
                res.json( {"data":"success","status":200,"message":"Account has been created"});
                application.deleteOne({email: req.params.email}, function(err, result) { });
                //Sends the SMS
                twilio.messages.create({body: `Hey ${application.first_name}, we are just messaging you to let you know your account has been approved! Login to our panel @ https://ntukfc.me`, from: '+447782567214', to: application.phone_no});
            } catch (err) {
                res.status(400).send(err);
            }
        }else {
            res.status(403).json({"data":"Unauthorized", "status":403})
        }
    });  
});

module.exports = router;