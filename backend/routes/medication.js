const router = require('express').Router();
const Medication = require('../models/Medication');
const verify = require('./verifyToken');
const jwt = require ('jsonwebtoken');

router.post('/new', verify, async (req, res) => {
    console.log('[Information] Incoming Post Request New Medication');

    const auth_token = req.headers["auth-token"];
    jwt.verify(auth_token, process.env.TOKEN_SECRET, async function(err, decoded) { 
        if (err) return res.status(400).json({"error":"corrupted token", "status":400});

        const groups = decoded.groups;
        if (groups.includes("admin") || groups.includes("pharmacist")) {
            console.log(req.body)
            if (!req.body.med_name) 
                return res.status(400).json({"error":"Invalid Request. Requires: med_name, med_blood_work, med_blood_work_days_without", "status":400});
        
            // Now lets create a new instance of Medication 
            const user = new Medication({
                med_name: req.body.med_name,
                med_blood_work: req.body.med_blood_work,
                med_blood_work_days_without: req.body.med_blood_work_days_without,
            });
        
            try {
                const savedMedication = await user.save();
                res.json( {"data":"success","status":200,"message":"Medication has been added to the database"});
            } catch (err) {
                res.status(400).send(err);
            }
        } else {
            res.status(403).json({"data":"Unauthorized", "status":403})
        }
    });
});

router.get('/', verify, async(req, res) => {
    Medication.find({}, function(err, meds) {
        res.json({"data":"success","status":200, "size": meds.length,"medications": meds});
     });
});

module.exports = router;