const router = require('express').Router();
const User = require('../models/User');
const Account_Requests = require('../models/Account_Requests');
const jwt = require ('jsonwebtoken');
const bcrypt = require('bcryptjs');

router.post('/request_access', async (req, res) => {
    console.log('[Information] Incoming Post Request For Access');

    if (!req.body.email || !req.body.phone_no || !req.body.password || !req.body.reason || !req.body.first_name || !req.body.last_name) 
        return res.status(400).json({"error":"Invalid Request. Requires: first_name, last_name, email, phone_no, password, reason, nhs_number", "status":400});

    //Checks to see if the users email is already in the database
    const emailExists = await Account_Requests.findOne({email: req.body.email});
    if(emailExists) 
       return res.status(400).json({"error":"An account has already requested to join with that email!", "status":400});

    const emailExistsInUser = await User.findOne({email: req.body.email});
    if(emailExistsInUser) 
        return res.status(400).json({"error":"An account has already requested to join with that email!", "status":400});
   

    //Hash the requested password from the user. We don't need to see that...
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.password, salt);

    // Now lets create a new instance of a request 
    const user = new Account_Requests({
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        email: req.body.email,
        phone_no: req.body.phone_no,
        password: hashedPassword,
        reason: req.body.reason,
        nhs_number: req.body.nhs_number
    });

    try {
        const savedUser = await user.save();
        res.json( {"data":"success","status":200,"message":"Thank you, we have your request."});
    } catch (err) {
        res.status(400).send(err);
    }
});


router.get('/', (req, res) => {
    res.json({error: 'Uh Oh.... You 2Heads have not implimented this..'})
});

router.post('/login', async (req, res) => {
    console.log('[Information] Incoming Post Request For Login Attempt');

    //Email Check With DB
    const user = await User.findOne({email: req.body.email});
    if(!user) return res.status(401).json({"error":"no_account_found", "status":401,"title":"Unauthorized", "error_description": "Invalid username and password combination"});

    //Password Match With DB
    const validPassword = await bcrypt.compare(req.body.password, user.password);
    if(!validPassword) return res.status(401).json({"error":"no_account_found", "status":401,"title":"Unauthorized", "error_description": "Invalid username and password combination"});

    //Lets do some JWT token stuff
    const token = jwt.sign({_id: user._id, email: req.body.email, groups: user.groups}, process.env.TOKEN_SECRET);
    res.header('access_token', token).json({"access_token": token});

});

module.exports = router;