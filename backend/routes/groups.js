const router = require('express').Router();
const verify = require('./verifyToken');
const Group = require('../models/Group');
const jwt = require ('jsonwebtoken');
const bcrypt = require('bcryptjs');
router.get('/', verify ,(req, res) => {
    Group.find({}, function(err, groups) {
        res.json({"data":"success","status":200, "size": groups.length,"groups": groups});
     }).select('-__v').select('-_id');
});

router.post('/new', verify, async (req, res) => {
    console.log('[Information] Incoming Post Request For Creating New Group');

    const auth_token = req.headers["auth-token"];
    jwt.verify(auth_token, process.env.TOKEN_SECRET, async function(err, decoded) { 
        if (err) return res.status(400).json({"error":"corrupted token", "status":400});

        const groups = decoded.groups;
        if (groups.includes("admin")) {
            if (!req.body.group_name || !req.body.group_perms) 
            return res.status(400).json({"error":"Invalid Request. Requires: group_name, group_perms", "status":400});
        
            const userGroup = new Group({
                group_name: req.body.group_name,
                group_perms: req.body.group_perms
        
            });
        
            try {
                const savedGroup = await userGroup.save();
                res.json( {"data":"success","status":200,"message":"Group has been created"});
            } catch (err) {
                res.status(400).send(err);
            }
        } else {
            res.status(403).json({"data":"Unauthorized", "status":403})
        }

    });

});

module.exports = router;