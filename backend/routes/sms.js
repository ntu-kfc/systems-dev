const router = require('express').Router();
const verify = require('./verifyToken');
const User = require('../models/User');
const dotenv = require('dotenv');
const jwt = require ('jsonwebtoken');
const bcrypt = require('bcryptjs');
dotenv.config();
const twilio = require('twilio')(process.env.TWILIO_ACCOUNT_SID, process.env.TWILIO_AUTH_TOKEN);
const MessagingResponse = require('twilio').twiml.MessagingResponse;
const bodyParser = require('body-parser');

router.use(bodyParser.urlencoded({ extended: false }));

router.post('/send/:email', verify, async (req, res) => {
    console.log('[Information] Incoming Post Request To Send SMS');
    const auth_token = req.headers["auth-token"];
    jwt.verify(auth_token, process.env.TOKEN_SECRET, async function(err, decoded) { 
        if (err) return res.status(400).json({"error":"corrupted token", "status":400});

        const groups = decoded.groups;
        if (groups.includes("admin") || groups.includes("pharmacist") || groups.includes("technician")) { 
            if (!req.body.message) 
                return res.status(400).json({"error":"Invalid Request. Requires: message", "status":400});
        
            //Email Check With DB
            const user = await User.findOne({email: req.params.email});
            if(!user) 
                return res.status(400).json({"error":"email not found!", "status":400});
        
            //Sends the SMS
            twilio.messages.create({
            body: req.body.message,
            from: '+447782567214',
            to: user.phone_no
            }).then(message => res.json({"data":"success","status":200,"sms": message}));
        } else {
            res.status(403).json({"data":"Unauthorized", "status":403})
        }
    });
});

router.post('/receive', async (req, res) => {
    console.log('[Information] Incoming Post Request To GET SMS');
    const twiml = new MessagingResponse();

    if (req.body.Body.toLowerCase() == "y") {
        
        twiml.message('Thats all booked for you then. We will see you then.');
    } else if (req.body.Body.toLowerCase() == "n") {
        twiml.message('Sorry to hear you couldnt make it. Please call us ASAP to book an appointment that suites you.');
    } else {
        twiml.message('Please reply with either (Y) or (N)');
    }

    res.writeHead(200, {'Content-Type': 'text/xml'});
    console.log(req.body);
    res.end(twiml.toString());

});

module.exports = router;