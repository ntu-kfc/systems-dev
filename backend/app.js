const express = require('express')
const app = express()
const http = require('http').createServer(app);
const mongoose = require('mongoose');
const dotenv = require('dotenv');
var cors = require('cors');
////// API ROUTES /////////
const authRoute = require('./routes/authentication');
const accountRoute = require('./routes/account');
const smsRoute = require('./routes/sms');
const medicationRoute = require('./routes/medication');
const perscriptionRoute = require('./routes/perscription');
const groupsRoute = require('./routes/groups');
///////////////////////////

dotenv.config();
app.use(cors());

//MongoDB Connection
mongoose.connect(process.env.DB_CONNECT, {useNewUrlParser: true, useUnifiedTopology: true}, () => console.log('[Information] Connected to MongoDB Cluster.'));
mongoose.set('useFindAndModify', false);
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

//Middleware
app.use(express.json());
app.use(express.static('public'));
app.disable('etag');

app.use('/api/auth', authRoute);
app.use('/api/account', accountRoute);
app.use('/api/sms', smsRoute);
app.use('/api/medication', medicationRoute);
app.use('/api/perscription', perscriptionRoute);
app.use('/api/groups', groupsRoute);

app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + 'public'));
});


http.listen(process.env.PORT, () => console.log(`[Information] API Service Started on port: ${process.env.PORT}`))